package test1;

import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer buffer1 = new StringBuffer("StringBuffer");
    StringBuffer buffer2 = new StringBuffer("StringBufferStringBuffer");
    StringBuffer buffer3 = new StringBuffer("StringBufferStringBufferStringBuffer");

    @Test
    public void testcharAt() throws Exception {
        assertEquals('S', buffer1.charAt(0));
        assertEquals('i', buffer2.charAt(3));
        assertEquals('f', buffer3.charAt(9));
    }
    @Test
    public void testlength() throws Exception {
        assertEquals(12, buffer1.length());
        assertEquals(24, buffer2.length());
        assertEquals(36, buffer3.length());
    }
    @Test

    public void testcapacity() throws Exception {
        assertEquals(28, buffer1.capacity());
        assertEquals(40, buffer2.capacity());
        assertEquals(52, buffer3.capacity());
    }
    @Test

    public void testindexOf() throws Exception{
        assertEquals(0,buffer1.indexOf("St"));
        assertEquals(6,buffer1.indexOf("Bu"));
    }
}