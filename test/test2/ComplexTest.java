package test2;

import junit.framework.TestCase;
import org.junit.Test;
import test1.Complex;

public class ComplexTest extends TestCase {
    test1.Complex a=new test1.Complex(0,3);
    test1.Complex b=new test1.Complex(-1,-1);
    test1.Complex c=new Complex(-1,-1);

    @Test
    public void testComplexAdd()throws Exception{
        assertEquals("-1.0+2.0i",a.ComplexAdd(b).toString());
    }

    @Test
    public void testComplexSub()throws Exception{
        assertEquals("1.0+4.0i",a.ComplexSub(b).toString());
    }

    @Test
    public void testComplexMulti()throws Exception{
        assertEquals("3.0-3.0i",a.ComplexMulti(b).toString());
    }

    @Test
    public void testComplexDiv()throws Exception{
        assertEquals("-1.5-1.5i",a.ComplexDiv(b).toString());
    }
    @Test
    public void testequals()throws Exception{
        assertEquals(true,b.equals(c));
    }

}