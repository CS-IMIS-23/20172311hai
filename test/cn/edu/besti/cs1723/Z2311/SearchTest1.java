package cn.edu.besti.cs1723.Z2311;

public class SearchTest1 {
    public static void main(String[] args) {
        Integer[] array={1,32,11,33,45,23,14};

        System.out.println();
        System.out.println("原数组");
        for (Comparable num1:array)
            System.out.print(num1+" ");
        System.out.println();
        System.out.println("是否有1："+Searching.linearSearch(array,0,6,1));
        System.out.println("是否有3："+Searching.linearSearch(array,0,6,3));
    }
}
