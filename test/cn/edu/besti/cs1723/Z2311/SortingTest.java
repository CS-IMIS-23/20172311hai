package cn.edu.besti.cs1723.Z2311;
import org.junit.Test;
import static org.junit.Assert.*;

public class SortingTest {
    Integer[]a1={1,2,3,4,5,6,7,8,2311};
    Integer[]a2={2,3,5,67,76,77,89,2311};
    Character[]a3={'1','2','3','4','a','b'};

    Integer[]b1={2311,8,7,6,5,4,3,2,1};
    Integer[]b2={2311,89,77,76,67,5,3,2};
    Character[]b3={'b','a','4','3','2','1'};

    Integer[]c1={2,34,21,32,1,45,2311,34};
    Integer[]c2={30,3,25,34,12,4,2311,38};
    Integer[]c3={5,324,2,21,11,42,2311,3};
    Integer[]c4={2311,32,1,13,45,23,35,324};

    Comparable[]d1={1,34,2311,'2'};

    //正序
    @Test
    public void selectionSort1() throws Exception {
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 2311]", Sorting.selectionSort(a1));
    }
    @Test
    public void selectionSort2() throws Exception {
        assertEquals("[2, 3, 5, 67, 76, 77, 89, 2311]",Sorting.selectionSort(a2));
    }
    @Test
    public void selectionSort3() throws Exception {
        assertEquals("[1, 2, 3, 4, a, b]",Sorting.selectionSort(a3));
    }
    //逆序
    @Test
    public void selectionSort4() throws Exception {
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 2311]",Sorting.selectionSort(b1));
    }
    @Test
    public void selectionSort5() throws Exception {
        assertEquals("[2, 3, 5, 67, 76, 77, 89, 2311]",Sorting.selectionSort(b2));
    }
    @Test
    public void selectionSort6() throws Exception {
        assertEquals("[1, 2, 3, 4, a, b]",Sorting.selectionSort(b3));
    }
    //正常
    @Test
    public void selectionSort7() throws Exception {
        assertEquals("[1, 2, 21, 32, 34, 34, 45, 2311]",Sorting.selectionSort(c1));
    }
    @Test
    public void selectionSort8() throws Exception {
        assertEquals("[3, 4, 12, 25, 30, 34, 38, 2311]",Sorting.selectionSort(c2));
    }
    @Test
    public void selectionSort9() throws Exception {
        assertEquals("[2, 3, 5, 11, 21, 42, 324, 2311]",Sorting.selectionSort(c3));
    }
    @Test
    public void selectionSort10() throws Exception {
        assertEquals("[1, 13, 23, 32, 35, 45, 324, 2311]",Sorting.selectionSort(c4));
    }
    //异常
    @Test
    public void selectionSort11() throws Exception {
        assertEquals("[1, 2, 34, 2311]",Sorting.selectionSort(d1));
    }





}