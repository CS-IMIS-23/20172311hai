package cn.edu.besti.cs1723.Z2311;

public class SortTest1 {
    public static void main(String[] args) {
        Comparable[]array1={1,3,4,13,32,42,52,113,2311};
        Comparable[]array2={2311,113,52,42,32,13,4,3,1};
        Comparable[]array3={23,11,234,45,56,7,34,21,1,2311,9};

        System.out.println("原数组1");
        for (Comparable num1:array1)
            System.out.print(num1+" ");
        System.out.println("选择排序结果如下：");
        System.out.println(Sorting.selectionSort(array1));

        System.out.println("原数组2");
        for (Comparable num2:array2)
            System.out.print(num2+" ");
        System.out.println("选择排序结果如下：");
        System.out.println(Sorting.selectionSort(array2));

        System.out.println("原数组3");
        for (Comparable num3:array3)
            System.out.print(num3+" ");
        System.out.println("选择排序结果如下：");
        System.out.println(Sorting.selectionSort(array3));
    }
}
