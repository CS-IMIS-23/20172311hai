package cn.edu.besti.cs1723.Z2311;
import org.junit.Test;
import static org.junit.Assert.*;

public class SearchingTest {

    Integer[]a={2311,80,10,13,45,1998,12345678};
    String []b={"2311","123","e2e","qwdeq","qwe"};
    Integer[]c=null;

    //正常
    @Test
    public void linearSearch1() throws Exception {
        assertEquals(true, Searching.linearSearch(a,0,6,12345678) );
    }
    @Test
    public void linearSearch2() throws Exception {
        assertEquals(false,Searching.linearSearch(a,0,1,10) );
    }
    @Test
    public void linearSearch3() throws Exception {
        assertEquals(true,Searching.linearSearch(b,0,2,"e2e") );
    }
    @Test
    public void linearSearch4() throws Exception {
        assertEquals(false,Searching.linearSearch(b,0,4,"1") );
    }

    //边界
    @Test
    public void linearSearch5() throws Exception {
        assertEquals(false,Searching.linearSearch(a,1,6,2311) );
    }
    @Test
    public void linearSearch6() throws Exception {
        assertEquals(false,Searching.linearSearch(a,0,5,12345678) );
    }
    @Test
    public void linearSearch7() throws Exception {
        assertEquals(true,Searching.linearSearch(b,2,5,"1") );
    }
    @Test
    public void linearSearch8() throws Exception {
        assertEquals(true,Searching.linearSearch(b,0,2,"123") );
    }

    //异常
    @Test
    public void linearSearch9() throws Exception {
        assertEquals(true,Searching.linearSearch(a,0,6,"2311") );
    }
    @Test
    public void linearSearch10() throws Exception {
        assertEquals(true,Searching.linearSearch(a,0,3,12345678) );
    }
    @Test
    public void linearSearch11() throws Exception {
        assertEquals(true,Searching.linearSearch(c,0,3,12345678) );
    }

}