package chap9;

import chap5.Coin;

/*
MonetaryCoin.java            Author:zhaoxiaohai
由Coin类派生，在该类中声明一个变量保存硬币面值，添加一个方法返回硬币面值。
 */
public class MonetaryCoin extends Coin {
     double value;
     public  MonetaryCoin(double value){
     this.value=value;
     }

public double getValue(){
         return value;
    }
}
