package chap9;
/*
MonetaryCoinTest.java            Author:zhaoxiaohai
实例化MonetaryCoin类
 */
public class MonetaryCoinTest {
    public static void main(String[] args) {
        double add = 0;

        MonetaryCoin[] coin = new MonetaryCoin[3];
        coin[0] = new MonetaryCoin(0.1);
        coin[1]=new MonetaryCoin(0.5);
        coin[2]=new MonetaryCoin(1.0);

        for(int a=0;a<coin.length;a++)
            add +=coin[a].getValue();

        System.out.println("硬币面值之和为： "+add);
        System.out.println("Coin数组中第一个硬币的面值是： "+coin[0].getValue());

        coin[0].flip();
        System.out.println("对硬币1使用一次flip方法后输出face： ");
        System.out.println(coin[0].getFace());

        System.out.println("toString结果为： "+coin[0]);
    }
}
