package chap9;

//****************************************************************
//  SchoolBook2.java              Author：zhaoxiaohai
//
//继承Book2类生成新类，构造方法接收一个年龄参数，提供一个level方法来
//返回一些字符串
//****************************************************************

public class SchoolBook2 extends Book2{

    private int ageLevel;

    public SchoolBook2(int numPages, int age) {
        super(numPages);
        ageLevel=age;
    }

    //--------------------------------------------------------------------
    //  提供level方法，返回以下字符串。
    //--------------------------------------------------------------------
    public String level() {
        if (ageLevel >= 4 && ageLevel <= 6)
            return "Pre-school";
        else
        if (ageLevel <= 9)
            return "Early";
        else
        if (ageLevel <= 12)
            return "Middle";
        else
            return "Upper";
    }

}