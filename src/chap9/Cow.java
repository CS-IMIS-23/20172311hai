package chap9;

public class Cow extends Animal {
    public Cow(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println(name+""+id+"号正在吃");
    }

    @Override
    public void sleep() {
        System.out.println(name+""+id+"号正在睡");

    }

    @Override
    public void introduction() {
        System.out.println("大家好我是"+name+""+id+"号");

    }
}




