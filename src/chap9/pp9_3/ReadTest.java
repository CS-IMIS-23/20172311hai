package chap9.pp9_3;
/*
ReadTest.java             Author:zhaoxiaohai
实例化pp9.3中由ReadMaterial类派生出来的子类
 */
public class ReadTest {
    public static void main(String[] args) {
        Books a=new Books("A","asd",3,"dqwd");
        Novels b = new Novels("asd","ad",234,"wd");
        AcademicJournals c =new AcademicJournals("ad","sadw",44,"dwsdw");
        Magazines d=new Magazines("ad","ddw",3242,"2018.3.14");
        Textbooks e=new Textbooks("qwe","dwqwd",2345,5);

        System.out.println(a+"\n\n"+b+"\n\n"+c+"\n\n"+d+"\n\n"+e);
    }
}
