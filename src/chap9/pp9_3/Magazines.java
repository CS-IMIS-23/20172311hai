package chap9.pp9_3;
/*
Books.java      Author:zhaoxiaohai
继承ReadMaterial类
 */
public class Magazines extends ReadMaterial{
    protected String PublishDate;
    public Magazines(String name1,  String keyword1, int page,String PublishDate) {
        super(name1,  keyword1, page);
        this.PublishDate=PublishDate;
    }
    public String toString() {
        return "杂志名: " + name + " \n关键词： " + keyword + " \n页数： " + pages+"\n出版日期： "+PublishDate;
    }
}
