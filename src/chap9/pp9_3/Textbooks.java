package chap9.pp9_3;

/*
Books.java          Author:zhaoxiaohai
继承ReadMaterial类
 */
public class Textbooks extends ReadMaterial {
    protected int version;

    public Textbooks(String name1, String keyword1, int page, int version) {
        super(name1, keyword1, page);
        this.version = version;
    }

    public String toString() {
        return "教材名: " + name + " \n关键词： " + keyword + " \n页数： " + pages + "\n版本： " + version;
    }
}
