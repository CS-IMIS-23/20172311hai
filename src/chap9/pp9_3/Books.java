package chap9.pp9_3;

/*
Books.java      Author:zhaoxiaohai
继承ReadMaterial类
 */
public class Books extends ReadMaterial {
    protected  String booktype;

    public Books( String name1, String keyword1, int page,String 类型) {
        super(name1, keyword1, page);
        booktype=类型;
    }

    public String toString(){
            return "图书名: "+name+" \n关键词： "+keyword+" \n页数： "+pages+"\n图书类型： "+booktype;
    }
}

