package chap9.pp9_3;
/*
ReadMaterial.java          Author:zhaoxiaohai
创建一个Read类，用于派生各种类型书目的类
 */
public class ReadMaterial {

    protected int pages;
    protected String name;
    protected String keyword;

    public ReadMaterial( String name1, String keyword1, int page)
    {
        name=name1;
        keyword=keyword1;
        pages = page;
    }

    public void setName(String a)
    {
        name = a;
    }

    public String getName()
    {
        return name;
    }

    public void setKeyword(String b)
    {
        keyword=b;
    }

    public String getKeyword()
    {
        return keyword;
    }

    public void setPages(int numPages)
    {
        pages = numPages;
    }

    public int getPages()
    {
        return pages;
    }
}
