package chap9.pp9_3;

/*
Books.java      Author:zhaoxiaohai
继承ReadMaterial类
 */
public class AcademicJournals extends ReadMaterial {
    protected String publisher;
    public AcademicJournals(String name1, String keyword1, int page,String publisher) {
        super(name1, keyword1, page);
        this.publisher=publisher;
    }
    public String toString(){
        return "学术刊物名: "+name+" \n关键词： "+keyword+" \n页数： "+pages+"\n出版社： "+publisher;
    }
}
