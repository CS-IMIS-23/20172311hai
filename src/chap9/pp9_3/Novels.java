package chap9.pp9_3;
/*
Books.java      Author:zhaoxiaohai
继承ReadMaterial类
 */
public class Novels extends ReadMaterial{
    protected  String author;
    public Novels(String name1,String keyword1,int page,String author) {
        super(name1, keyword1, page);
        this.author=author;
    }

    public String toString() {
        return "小说名: " + name +  " \n关键词： " + keyword + " \n页数： " + pages+"/n作者： "+author;
    }
}
