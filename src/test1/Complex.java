package test1;
/*
Complex.java         Author:zhaoxiaohai
第八周实验（4），创建一个复数类Complex，并实现所要求的方法。
 */

public class Complex {
        private double r;
        private double i;

        //构造函数
        public Complex(double r, double i) {
            this.r = r;
            this.i = i;
        }
        //构造setter
        public void setRealPart(double R) {
            r=R;
        }

        public void setImagePart(double I) {
            i=I;
        }
        //构造getter
        public double getRealPart(){
            return r;
        }
        public double getImagePart(){
            return i;
        }
        //构造加法
        public Complex ComplexAdd(Complex c) {
            return new Complex(r+c.getRealPart(),i+c.getImagePart());
        }
        //构造减法
        public Complex ComplexSub(Complex c) {
            return new Complex(r-c.getRealPart(),i-c.getImagePart());
        }
        //构造乘法
        public Complex ComplexMulti(Complex c) {
            return new Complex(r * c.getRealPart() - i * c.getImagePart(), r * c.getImagePart() + i * c.getRealPart());
        }
        //构造除法
        public Complex ComplexDiv(Complex c) {
            return new Complex((r * c.getImagePart() + i * c.getRealPart()) / (c.getImagePart() * c.getImagePart() + c.getRealPart() * c.getRealPart()), (i * c.getImagePart() + r * c.getRealPart()) / (c.getImagePart() * c.getImagePart() + c.getRealPart() * c.getRealPart()));
        }
        //构造判断是否相等
        public boolean equals(Complex c){

            if (r==c.getRealPart()&&i==c.getImagePart())
                return  true;
            else
                return false;
        }

        public String toString() {
            String s = " ";
            if (i > 0)
                s = r + "+" + i + "i";
            if (i == 0)
                s = r + "";
            if (i < 0)
                s = r +"" + i + "i";
            return s;
        }

}

