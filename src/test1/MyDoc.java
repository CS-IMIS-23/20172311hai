package test1;
/*
MyDoc.java          Author:zhaoxiaohai
第八周实验（3）
 */

// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Double extends Data{
    double value;
    Double() {
        value = 82.8;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class DoubleFactory extends Factory{
    public Data CreateDataObject(){
        return new Double();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d,e;
    public static void main(String[] args) {
        d = new Document(new IntFactory());
        e= new Document(new DoubleFactory());
        d.DisplayData();
        e.DisplayData();
    }
}