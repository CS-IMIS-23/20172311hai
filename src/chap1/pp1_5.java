//*****************************************************************
//消愁的歌词                           作者：赵晓海
//
// 用来显示消愁的一段歌词
//*****************************************************************
  
   class pp1_5
   { 
   public static void main(String [] args)
  {
   System.out.println("消愁第二段歌词");
   System.out.println();
   System.out.println();
   System.out.println(
                     "一杯敬朝阳 一杯敬月光"+
                     "\n唤醒我的向往 温柔了寒窗"+
                     "\n于是可以不回头地逆风飞翔"+
                     "\n不怕心头有雨 眼底有霜"+
                     "\n一杯敬故乡 一杯敬远方"+
                     "\n守着我的善良 催着我成长"+
                     "\n\"所以南北的路从此不再漫长\""+
                     "\n\"灵魂不再无处安放\"");
    System.out.println();
    System.out.println();
    System.out.println("注： \" \"内的歌词是和声部分");
   }
}
