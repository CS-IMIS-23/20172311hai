//*****************************************************************
//pp1_9.java               作者：赵晓海
//
// 输出一个菱形。
//*****************************************************************
  
  public class pp1_9
  {
   public static void main(String [] args)
   { 
    System.out.println("        *");
    System.out.println("       ***");
    System.out.println("      *****");
    System.out.println("     *******");
    System.out.println("    *********");
    System.out.println("     *******");
    System.out.println("      *****");
    System.out.println("       ***");
    System.out.println("        *");
   }
}
