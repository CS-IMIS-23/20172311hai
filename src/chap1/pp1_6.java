//*************************************************************
//pp1_6.java          作者：赵晓海
//
// 用"*"显示一棵树的轮廓。
//*************************************************************
  class pp1_6
  {
  public static void main(String [] args)
 {
  System.out.println("    *");
  System.out.println("   * *");
  System.out.println("  *   *");
  System.out.println(" *     *");
  System.out.println("*********");
  System.out.println("  *   *");
  System.out.println("  *   *");
  System.out.println("  *****");
   }
}
