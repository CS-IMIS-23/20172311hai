package chap13;

//*******************************************************************
//  MagazineRack.java       Author: Lewis/Loftus
//
//  Driver to exercise the MagazineList collection.
//*******************************************************************


public class MagazineRack
{
    public static void main(String[] args)
    {
        MagazineList rack = new MagazineList();

        rack.add(new Magazine("Time"));
        rack.add(new Magazine("Woodworking Today"));
        rack.add(new Magazine("Communications of the ACM"));
        rack.add(new Magazine("Reader"));

        System.out.println("初始的: \n"+rack);

        rack.insert(3,new Magazine("Ba"));
        rack.insert(0,new Magazine("aaa"));//插到第一个
        rack.insert(6,new Magazine("a"));//插到最后一个
        System.out.println("插入两个后: \n"+rack);

        rack.delete(new Magazine("aaa"));

        System.out.println("删除插入的第二个后: \n"+rack);

        System.out.println("排序后： ");
        rack.Sort();
    }
}