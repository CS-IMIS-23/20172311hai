package chap13;

import chap8.DVD;

import java.text.NumberFormat;
import java.util.LinkedList;

public class DVDList {
    private LinkedList<DVD> collection;
    private int count;
    private double totalCost;

    //----------------------------------------------------------------------
    //  Constructor: Creates an intially empty collection.
    //--------------------------------------------------------------------
    public DVDList()
    {
        collection=new LinkedList<DVD>();
        count = 0;
        totalCost = 0.0;
    }

    //-------------------------------------------------------------------------------------
    //  Adds a DVD to the collection, increasing the size of the collection array if
    //  necessary .
    //------------------------------------------------------------------------------------
    public void addDVD(String title, String director, int year, double cost, boolean bluray)
    {

        collection.add(new DVD(title,director,year,cost,bluray));
        totalCost += cost;
        count++;
    }

    //----------------------------------------------------------------------------------------------------
    //Returns a report describing the DVD collection.
    //----------------------------------------------------------------------------------------------------
    public String toString() {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();

        String report = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        report += "My DVD Collection\n\n";

        report += "Number of DVDs:" + count + "\n";
        report += "Total cost:" + fmt.format(totalCost) + "\n";
        report += "Average cost:" + fmt.format(totalCost / count);
        report += "\n\nDVD List:\n\n";

        for (int a=0;a<collection.size();a++)
        report+=collection.get(a).toString()+"\n";
        return report;



    }

}

