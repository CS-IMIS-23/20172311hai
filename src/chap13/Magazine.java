package chap13;

//********************************************************************
//  Magazine.java       Author: Lewis/Loftus
//
//  Represents a single magazine.
//********************************************************************

public class Magazine implements Comparable {
    private String title;

    //-----------------------------------------------------------------
    //  Sets up the new magazine with its title.
    //-----------------------------------------------------------------
    public Magazine(String newTitle) {
        title = newTitle;
    }


    public String getTitle() {
        return title;
    }


    public int compareTo(Object magazine) {
        int result;
        result = title.compareTo(((Magazine) magazine).getTitle());
        return result;
    }


    //-----------------------------------------------------------------
    //  Returns this magazine as a string.
    //-----------------------------------------------------------------
    public String toString() {
        return title;
    }


}

