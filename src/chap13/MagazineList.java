package chap13;

//*******************************************************************
//  MagazineList.java       Author: Lewis/Loftus
//
//  Represents a collection of magazines.
//*******************************************************************

import chap10.Sorting;

import java.util.ArrayList;

public class MagazineList {
    private MagazineNode list;

    //----------------------------------------------------------------
    //  Sets up an initially empty list of magazines.
    //----------------------------------------------------------------
    public MagazineList() {
        list = null;
    }

    //----------------------------------------------------------------
    //  Creates a new MagazineNode object and adds it to the end of
    //  the linked list.
    //----------------------------------------------------------------
    public void add(Magazine mag) {
        MagazineNode node = new MagazineNode(mag);
        MagazineNode current;

        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    /*
    在index的位置插入新节点newMagazine
     */
    public void insert(int index, Magazine newMagazine) {
        MagazineNode node = new MagazineNode(newMagazine);
        MagazineNode current = list;

        if (list == null)
            list = node;
        else {
            if (index == 0) {
                node.next = current;
                list = node;
            } else if (index == 1) {
                node.next = current.next;
                current.next = node;

            } else {
                current = list;

                for (int a = 0; a < index - 1; a++)

                    current = current.next;
                node.next = current.next;
                current.next = node;
            }
        }


    }

    /*
    删除节点delNode

    */
    public void delete(Magazine delNode) {
        MagazineNode front = list;
        MagazineNode current = front.next;

        if (delNode.toString().equals(front.magazine.toString()))
            list = front.next;
        else {
            while (!current.magazine.toString().equals(delNode.toString()))
                front = front.next;
            front.next = current.next;
        }

    }
   /* public void delete(Magazine delNode){
        MagazineNode node=new MagazineNode(delNode);
        MagazineNode current=list;
        MagazineNode behind=current;

        if (list==null)
            list=null;
        else
            if (current.magazine.toString().equals(node.toString()))
                current=current.next;
            else{
                while (!current.next.magazine.toString().equals(node.toString()))
                {
                    behind.next=current;
                    current=current.next;
                }
                current=current.next;
                behind.next=current.next;
                list=behind;
            }
    }
    */

    //----------------------------------------------------------------
    //  Returns this list of magazines as a string.
    //----------------------------------------------------------------
    public String toString() {
        String result = "";

        MagazineNode current = list;

        while (current != null) {
            result += current.magazine + "\n";
            current = current.next;
        }

        return result;
    }

    /*
    排序
     */
    public void Sort() {
        int n = 0;
        ArrayList<Comparable> magazines = new ArrayList();
        while (list != null) {
            magazines.add(n, list.getTitle());
            list = list.next;
            n++;
        }
        n = 0;
        Comparable[] list = new Comparable[magazines.size()];
        while (n < magazines.size()) {
            list[n] = magazines.get(n);
            n++;
        }
        Sorting.insertionSort(list);
        for (Comparable a : list)
            System.out.println(a);
    }

    //*****************************************************************
//  An inner class that represents a node in the magazine list.
//  The public variables are accessed by the MagazineList class.
//*****************************************************************
    private class MagazineNode {
        public Magazine magazine;
        public MagazineNode next;

        //--------------------------------------------------------------
        //  Sets up the node
        //--------------------------------------------------------------
        public MagazineNode(Magazine mag) {
            magazine = mag;
            next = null;

        }

        public String getTitle() {
            return magazine.getTitle();
        }
    }


}

