package chap13;


import java.util.LinkedList;

public class Sorting3 {
    //-----------------------------------------------------------------
    //  Sorts the specified array of objects using the selection
    //  sort algorithm.
    //-----------------------------------------------------------------
    public static void sortLink(LinkedList<Integer> link) {
        int min;
        int temp;

        for (int index = 0; index < link.size() - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < link.size(); scan++) {
                if (link.get(scan).compareTo(link.get(min)) < 0) {
                    min = scan;
                }
            }

            // Swap the values
            temp = link.get(min);
            link.set(min, link.get(index));
            link.set(index, temp);

        }
    }
}
