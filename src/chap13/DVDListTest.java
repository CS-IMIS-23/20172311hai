package chap13;

public class DVDListTest {
    public static void main(String[] args) {
        DVDList dvdList=new DVDList();
        dvdList.addDVD("a1","aa",2,22,false);
        dvdList.addDVD("a2","ac",21,2,true);
        dvdList.addDVD("a3","ad",3,12,false);
        dvdList.addDVD("a4","a",2,21,true);
        dvdList.addDVD("a5","aa",5,22,true);

        System.out.println(dvdList);

    }
}
