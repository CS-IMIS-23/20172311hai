package chap11;
/*
TooLongExceptionTest.java         Author:zahoxiaohai

可抛出StringTooLongException类异常，且允许抛出的异常终止程序
 */
import java.util.Scanner;

public class TooLongExceptionTest {
    public static void main(String[] args) throws StringTooLongException {
        String message2="";
        String message;

        Scanner scan =new Scanner(System.in);

        StringTooLongException problem=new StringTooLongException("Input string is to long.");

        do {
            System.out.print("请您输入字符串，我会将其记录并打印（注：输入DONE来终止）");
                message=scan.nextLine();
                message2+=message+"\n";
                if (message.length()>20)
                    throw problem;

        }
        while (!message.equals("DONE"));
        System.out.println("您输入的数据为："+"\n"+message2);

    }

}
