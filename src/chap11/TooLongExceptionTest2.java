package chap11;

import java.util.Scanner;

/*
TooLongExceptionTest2.java         Author:zhaoxiaohai

当StringTooLongException异常抛出时，捕获它并处理。
 */
public class TooLongExceptionTest2 {
    public static void main(String[] args) {
        String message="";
        String message2= "";
        Scanner scan =new Scanner(System.in);

        StringTooLongException problem=new StringTooLongException("Input string is to long.");

        do {
            System.out.print("请您输入字符串，我会将其记录并打印（注：输入DONE来终止）");
            try{
                message=scan.nextLine();
                message2+=message+"\n";
                if (message.length()>20)
                    throw problem;
            }
            catch (StringTooLongException a){
                System.out.print(a.toString());
            }
        }
        while (!message.equals("DONE"));
        System.out.println("您输入的数据为："+"\n"+message2);

    }
}