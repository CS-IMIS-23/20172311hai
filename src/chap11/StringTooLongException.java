package chap11;
/*
StringTooLongException.java                 Author:zhaoxiaohai

创建的这个异常类的作用是：当发现字符串中包含太多字符时抛出异常
 */
public class StringTooLongException extends Exception{

    StringTooLongException(String message)
    {
        super(message);

    }

}
