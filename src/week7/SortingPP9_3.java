package week7;

import java.util.Arrays;

/*
 pp9_3,在，在每一个排序法中添加代码使得他们能够对总的比较次数以及每一算法的总的执行时间进行计数
*/
public class SortingPP9_3
{

    //选择排序
    public static <T extends Comparable<T>> void selectionSort(T[] data)
    {
        long start=System.nanoTime();
        int count=0;
        int min;
        T temp;

        for (int index = 0; index < data.length-1; index++)
        {
            min = index;
            for (int scan = index+1; scan < data.length; scan++){
                if (data[scan].compareTo(data[min])<0) {
                    min = scan;
                    count++;
                }
                count++;
            }
            swap(data, min, index);
        }

        long finish=System.nanoTime();
        System.out.println("算法比较次数："+count+"\n"+"算法运行时间：" + (finish - start)+"纳秒");
    }

    //选择排序中用到的swap方法
    private static <T extends Comparable<T>> void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    //插入排序法
    public static <T extends Comparable<T>> void insertionSort(T[] data)
    {
        long start=System.nanoTime();
        int count=0;

        for (int index = 1; index < data.length; index++)
        {
            T key = data[index];
            int position = index;

            while (position > 0 && data[position-1].compareTo(key) > 0)
            {
                data[position] = data[position-1];
                position--;
                count++;
            }
            count++;
            data[position] = key;
        }

        long finish=System.nanoTime();
        System.out.println("算法比较次数："+count+"\n"+"算法运行时间：" + (finish - start)+"纳秒");
    }


    //冒泡排序法
    public static <T extends Comparable<T>>  void bubbleSort(T[] data)
    {
        long start=System.nanoTime();
        int count=0;
        int position, scan;
        T temp;

        for (position =  data.length - 1; position >= 0; position--)
        {
            for (scan = 0; scan <= position - 1; scan++)
            {
                if (data[scan].compareTo(data[scan+1]) > 0){
                    swap(data, scan, scan + 1);
                    count++;
                }
                count++;
            }
        }
        long finish=System.nanoTime();
        System.out.println("算法比较次数："+count+"\n"+"算法运行时间：" + (finish - start)+"纳秒");

    }

    //归并排序法
    public static <T extends Comparable<T>> void mergeSort(T[] data)
    {

        long start=System.nanoTime();
        mergeSort(data, 0, data.length - 1);
        long finish=System.nanoTime();
        System.out.println("算法比较次数："+count);
        System.out.println("算法运行时间：" + (finish - start)+"纳秒");
        count=0;

    }


    private static <T extends Comparable<T>> void mergeSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int mid = (min + max) / 2;
            mergeSort(data, min, mid);
            mergeSort(data, mid+1, max);
            merge(data, min, mid, max);

        }
    }

    private static int count;
    //归并排序法里的merge方法
    @SuppressWarnings("unchecked")
    private static <T extends Comparable<T>> void merge(T[] data, int first, int mid, int last)
    {
        T[] temp = (T[])(new Comparable[data.length]);

        int first1 = first, last1 = mid;
        int first2 = mid+1, last2 = last;
        int index = first1;

        while (first1 <= last1 && first2 <= last2)
        {
            if (data[first1].compareTo(data[first2]) < 0)
            {
                temp[index] = data[first1];
                first1++;
                count++;
            }
            else
            {
                temp[index] = data[first2];
                first2++;
                count++;
            }
            index++;

        }

        while (first1 <= last1)
        {
            temp[index] = data[first1];
            first1++;
            index++;
            count++;
        }

        while (first2 <= last2)
        {
            temp[index] = data[first2];
            first2++;
            index++;
            count++;
        }

        for (index = first; index <= last; index++)
            data[index] = temp[index];


    }


    //快速排序法
    public static <T extends Comparable<T>> void quickSort(T[] data)
    {
        long start=System.nanoTime();
        quickSort(data, 0, data.length - 1);
        long finish=System.nanoTime();

        System.out.println("算法比较次数："+count2);
        System.out.println("算法运行时间：" + (finish - start)+"纳秒");
        count2=0;
    }

    private static <T extends Comparable<T>> void quickSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int indexofpartition = partition(data, min, max);

            quickSort(data, min, indexofpartition - 1);

            quickSort(data, indexofpartition + 1, max);
        }
    }

    private static int count2;
    private static <T extends Comparable<T>> int partition(T[] data, int min, int max)
    {
        T partitionelement;
        int left, right;
        int middle = (min + max) / 2;

        partitionelement = data[middle];

        swap(data, middle, min);

        left = min;
        right = max;

        while (left < right)
        {

            while (left < right && data[left].compareTo(partitionelement) <= 0){
                left++;
                count2++;
            }

            while (data[right].compareTo(partitionelement) > 0){
                right--;
                count2++;
            }

            if (left < right)
                swap(data, left, right);
        }

        swap(data, min, right);

        return right;
    }


}

