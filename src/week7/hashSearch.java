package week7;

public class hashSearch {

    int num = 0; //num为元素个数
    int m;  // m为散列表长度
    int times = 0; //普通查找次数
    int conflictTime = 0;//解决冲突次数
    hashNode[] list ; // 元素存储

    //构造函数
    public hashSearch(int m)
    {
        this.m = m;
        list = new hashNode[m];
    }

    //求余方法
    public int toRemainder(int num)
    {
        int result= num % m;
        return result;
    }

    //存储元素的hash方法
    public void hashAdd(int[] elem)
    {
        int temp;
        int start = 0;
        while (start < elem.length)
        {
            temp = toRemainder(elem[start]);
            hashNode node = new hashNode(elem[start]);

            if (list[temp] == null)
                list[temp] = node;
            else
            {
                conflictTime++;
                hashNode current = list[temp];
                while (current.getNext()!= null)
                {
                    current = current.getNext();
                    conflictTime++;
                }
                current.setNext(node);

            }
            times++;
            num++;
            start++;
        }
    }

    //查找某个元素是否存在
    public boolean search(int[] elem, int num)
    {
        int temp;
        temp = toRemainder(num);

        if (list[temp] != null)
        {
            if (list[temp].getElement() == num)
            {
                return true;
            }
            else
            {
                hashNode current = list[temp];
                while (current.getNext() != null && current.getElement() != num)
                {
                    current = current.getNext();
                }
                return current.getElement() == num;
            }
        }
        else
        {
            return false;
        }
    }

    //返回冲突次数和平均查找长度ASL
    public void toASL()
    {
        double ASL = (double) (times + conflictTime)/ num;
        System.out.println("冲突次数：" + conflictTime);
        System.out.println("平均查找长度ASL：" + ASL);
    }
}
