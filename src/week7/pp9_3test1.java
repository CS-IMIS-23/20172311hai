package week7;
//对未排好的列表的排序
public class pp9_3test1 {
    public static void main(String[] args) {
        Integer[]a=new Integer[7];
        a[0]=4;
        a[1]=6;
        a[2]=9;
        a[3]=5;
        a[4]=3;
        a[5]=1;
        a[6]=10;

        System.out.println("选择排序：");
        SortingPP9_3.selectionSort(a);

        System.out.println("测试查找方法：");
        System.out.println(Searching.linearSearch(a,0,6,1));
        System.out.println(Searching.binarySearch(a,0,6,3));
//        System.out.println("插入排序：");
//        SortingPP9_3.insertionSort(a);
//        System.out.println("冒泡排序：");
//        SortingPP9_3.bubbleSort(a);
//        System.out.println("快速排序：");
//        SortingPP9_3.quickSort(a);
//        System.out.println("归并排序：");
//        SortingPP9_3.mergeSort(a);

    }
}
