package week7;

public class hashNode {
    private hashNode next;
    private int element;

    public hashNode(int elem)
    {
        element = elem;
        next = null;
    }

    public hashNode getNext() {
        return next;
    }

    public void setNext(hashNode next) {
        this.next = next;
    }

    public int getElement() {
        return element;
    }
}

