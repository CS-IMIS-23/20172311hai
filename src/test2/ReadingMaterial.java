package test2;

//*******************************************************************************************************************
//   ReadingMaterial.java                               Author:hyt
//   创建一个父类
//*******************************************************************************************************************
public class ReadingMaterial {
    protected double page;
    protected String keywords;
    protected String name;

    //创建构造方法
    public ReadingMaterial(double page, String keywords, String name) {
        this.page = page;
        this.keywords = keywords;
        this.name = name;
    }

    //创建getter
    public double getPage() {
        return page;
    }

    public String getKeywords() {
        return keywords;
    }

    public String getName() {
        return name;
    }

    //创建setter
    public void setPage(double page) {
        this.page = page;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override //生成toString
    public String toString() {
        return "ReadingMaterial{" +
                "page=" + page +
                ", keywords='" + keywords + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
