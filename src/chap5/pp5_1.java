//***********************************************************
//pp5_1.java         Aythor:zhaoxiaohai 
//
//用于读取用户输入的一个整数作为年份，并且确定该年份是否为闰年。
//***********************************************************
import java.util.Scanner;
public class pp5_1
{
  public static void main(String[] args)
  {
   final int LIMIT=1582;
   int year;
   Scanner scan=new Scanner(System.in);
   
   System.out.print("请您输入一个不小于1582的整数作为年份，我将为您判断它是否是闰年： ");
   year=scan.nextInt();
   
   int a=year%4;
   int b=year%100;
   int c=year%400;
   System.out.println("您输入的年份是： "+year);
   
   if(year>=LIMIT)
      if((a==0&&b!=0)||(b==0&&c==0))
         System.out.println(year+"年是闰年。");
      else
         System.out.println(year+"年不是闰年。");
   else
     System.out.println("在"+year+"年之前公历还未被采用，所以无法判断该年份是否为闰年。");
   }
}
