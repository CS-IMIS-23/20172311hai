/*
 pp5_7.java                Author:zhaoxiaohai
 
与程序进行石头剪刀布
*/
import java.util.*;
public class pp5_7
{
  public static void main(String[]args)
  {
  int computerwin=0;
  int peoplewin=0;
  int equal=0;
  String another="y";
 
  while(another.equalsIgnoreCase("y"))
  {
   ArrayList<String> game =new ArrayList<String>();
  
   game.add("石头");
   game.add("剪刀");
   game.add("布");
  
   int a=(int)(Math.random()*3);
   String str1=game.get(a);

   System.out.print("请您输入石头或者剪刀或者布");
   Scanner scan=new Scanner(System.in);
   String str2=scan.nextLine();
     System.out.println("程序的选择是："+str1);
     System.out.println("您的选择是： "+str2);
   
     if(str1.equals(str2))
      {
       equal++;
       System.out.println("平局");
      }
     else
      {
         if(str2.equals("石头")||str2.equals("剪刀")||str2.equals("布"))  
          {
            if((str1.equals("石头")&&str2.equals("剪刀"))||(str1.equals("剪刀")&&str2.equals("布"))||(str1.equals("布")&&str2.equals("石头")))
             {
              computerwin++;
              System.out.println("程序获胜");
             }
            else
             {
              peoplewin++;
              System.out.println("您获胜");
             }
            }
          else
             System.out.println("您输入的是"+str2+"请您输入石头剪刀或者布");
       }
     System.out.print("目前您赢了 "+peoplewin+" 局，程序赢了 "+computerwin+" 局，平局 "+equal+" 局，要继续吗(y/n)?");
     another = scan.nextLine();
     System.out.println();
   }
  
      System.out.println();
      System.out.println("您赢了 "+peoplewin+" 局，程序赢了 "+computerwin+" 局，平局 "+equal+" 局。");
 }
}
