/*
   pp5_3.java             Author:zhaoxiaohai 
  
  判断一个整数中包含的奇数，偶数和零数的个数。
*/

import java.util.Scanner;

public class pp5_3
{
  public static void main(String[]args)
  {
   int odd=0,even=0,zero=0,num1,num2;
   
   Scanner scan=new Scanner(System.in);
  
     System.out.print("请您输入一个整数： ");
     num1=scan.nextInt();
     num2=Math.abs(num1);
     String str=num2+"";
     int number1=0;
     int number2=str.length()-1;
    
      while(number1<=number2)
      {
       String str1=String.valueOf(str.charAt(number1)); 
       int a=Integer.parseInt(str1);
       number1++;
       if(a==0)
         zero++;
       else
         if(a%2==0&&a!=0)
           even++;
         else
           odd++;
      }       
     System.out.print(num1+"中有"+odd+"个奇数，"+even+"个偶数,"+zero+"个零数");
    
  }
}
