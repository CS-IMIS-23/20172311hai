package HuffmanTest;

import week10.SmallArrayHeap;
import week5.ArrayUnorderedList;
import week5.UnorderedListADT;
import java.util.ArrayList;
import java.util.Stack;

//哈夫曼树类
public class huffmanTree
{
    private huffmanNode root;	// 根结点
    private String[] codes = new String[26];

    public huffmanTree(huffmanNode[] huffmanArray) {
        huffmanNode parent = null;
        SmallArrayHeap<huffmanNode> heap = new SmallArrayHeap<>();
        for (int i=0;i<huffmanArray.length;i++)
        {
            heap.addElement(huffmanArray[i]);
        }

        for(int i=0; i<huffmanArray.length-1; i++) {
            huffmanNode left = heap.removeMin(); //左孩子是最小节点
            huffmanNode right = heap.removeMin();

            parent = new huffmanNode(left.getWeight()+right.getWeight(),' ',null,left,right);
            left.setParent(parent);
            right.setParent(parent);

            heap.addElement(parent);
        }

        root = parent;
    }

    @Override
    public String toString()
    {
        UnorderedListADT<huffmanNode> nodes =
                new ArrayUnorderedList();
        UnorderedListADT<Integer> levelList =
                new ArrayUnorderedList<Integer>();
        huffmanNode current;
        String result = "";
        int printDepth = this.getHeight()-1;
        int possibleNodes = (int)Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes)
        {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel)
            {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            }
            else
            {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)) ; i++)
                {
                    result = result + " ";
                }
            }
            if (current != null)
            {
                result = result + current.getWeight();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            }
            else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }
        }
        return result;
    }

    //得到高度
    public int getHeight()
    {
        return height(root);
    }
    private int height(huffmanNode node)
    {
        if(node==null){
            return 0;
        }
        else {
            int leftTreeHeight = height(node.getLeft());
            int rightTreeHeight= height(node.getRight());
            return leftTreeHeight>rightTreeHeight ? (leftTreeHeight+1):(rightTreeHeight+1);
        }
    }


    protected void inOrder( huffmanNode node,
                            ArrayList<huffmanNode> tempList)
    {
        if (node != null)
        {
            inOrder(node.getLeft(), tempList);
            if (node.getElement()!=' ')
                tempList.add(node);

            inOrder(node.getRight(), tempList);
        }
    }

    public String[] getEncoding() {
        ArrayList<huffmanNode> arrayList = new ArrayList();
        inOrder(root,arrayList);
        for (int i=0;i<arrayList.size();i++)
        {
            huffmanNode node = arrayList.get(i);
            String result ="";
            int x = node.getElement()-'a';
            Stack stack = new Stack();
            while (node!= root)
            {
                if (node==node.getParent().getLeft())
                    stack.push(0);
                if (node==node.getParent().getRight())
                    stack.push(1);

                node=node.getParent();
            }
            while (!stack.isEmpty())
            {
                result +=stack.pop();
            }
            codes[x] = result;
        }



        return codes;
    }

    public huffmanNode getRoot() {
        return root;
    }
}