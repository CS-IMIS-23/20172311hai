package HuffmanTest;

//哈夫曼树的节点类
public class huffmanNode implements Comparable<huffmanNode>
{
    private int weight;
    private huffmanNode parent,left,right;
    private char element;

    public huffmanNode(int weight, char element , huffmanNode parent, huffmanNode left, huffmanNode right) {
        this.weight = weight;
        this.element = element;
        this.parent = parent;
        this.left = left;
        this.right = right;
    }


    public int getWeight() {
        return weight;
    }
    @Override
    public int compareTo(huffmanNode huffmanNode) {
        return this.weight - huffmanNode.getWeight();
    }

    public huffmanNode getParent() {
        return parent;
    }

    public void setParent(huffmanNode parent) {
        this.parent = parent;
    }

    public huffmanNode getLeft() {
        return left;
    }

    public void setLeft(huffmanNode left) {
        this.left = left;
    }

    public huffmanNode getRight() {
        return right;
    }

    public void setRight(huffmanNode right) {
        this.right = right;
    }

    public char getElement() {
        return element;
    }

    public void setElement(char element) {
        this.element = element;
    }
}
