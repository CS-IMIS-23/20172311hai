//*******************************************************
//  Slogan.java          Author :zhaoxiaohai
//
//  Represents a single slogan string.
//*******************************************************
 
  public class Slogan
   {
  private String phrase;
  private static int count=0;
  //---------------------------------------------------------
  // Constructor: Set up the slogan and counts the number of
  // instances created.
  //-------------------------------------------------------
   
   public Slogan(String str)
   {
    phrase=str;
    count++;
    }
   //-------------------------------------------
   //Return this slogan as a string
   //-----------------------------------------
   public String toString()
   {
   return phrase;
   }
   //-----------------------------------------------
   // Returns the number of instances of this class
   // that hava been created.
   //------------------------------------------------
    public static int getCount()
   {
    return count;
    }
}
