//****************************************************************
// Transactions2.java          Author:zhaoxiaohai
//
//  Demonstrates the creation and use of multiple Account objects.
//*************************************************************
  
  public class Transactions2
  {
   //--------------------------------------------------
   //Creates some bank accounts and requests various services.
   //-------------------------------------------------
  public static void main(String[] args)
 {
  Account2 acct1=new Account2("Ted Murphy",72354);
  Account2 acct2=new Account2("Jane Smith",69713);
  Account2 acct3=new Account2("Edward Demsey",93757);
  Account2 acct4=new Account2("jhgu",67856); 
   
    acct1.setBalance(102.56);
    acct2.setBalance(40.00);
    acct3.setBalance(759.32);
  
   acct1.deposit(25.85);  
   double smithBalance=acct2.deposit(500.00);
   System.out.println("Smith balance after deposit: "+
                         smithBalance);
   
   System.out.println("Smith balance after withdrawal: "+
                         acct2.withdraw(430.75,1.50));

   acct1.addInterest();
   acct2.addInterest();
   acct3.addInterest();
   acct4.addInterest();
   
   System.out.println();
   System.out.println();
   System.out.println(acct1);
   System.out.println(acct2);
   System.out.println(acct3);
   System.out.println(acct4);
  }
}
