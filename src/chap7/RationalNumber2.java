//***************************************************************
// RationalNumber2.java         Author:zhaoxiaohai
// 
//Represents one rational number with a numerator and denominator
//***************************************************************
  public class RationalNumber2 implements java.lang. Comparable<RationalNumber2>
  {
  
   private int numerator,denominator;
//----------------------------------------------
//Constructor:Set up the rational number by ensuring a nonzero
//denominator and making only the numerator signed.
//-----------------------------------------------
public RationalNumber2(int numer,int denom)
{
 if (denom==0)
    denom=1;
 //Make the numerator "store" the sign
 if (denom<0)
  {
  numer=numer*-1;
  denom=denom*-1;
  }

 numerator=numer;
 denominator=denom;
 reduce();
 }
//--------------------------------------------------
// Returns the numerator of this rational number.
//-------------------------------------------------
public int getNumerator()
{
 return numerator;
}
//----------------------------------------------------
//Returns the denominator of this rational number.
//----------------------------------------------------
public int getDenominator()
{
 return denominator;
}
//--------------------------------------------------
//Returns the reciprocal of this rational number.
//-------------------------------------------------
public RationalNumber2 reciprocal()
{
 return new RationalNumber2(denominator,numerator);
}
//---------------------------------------------------------------------
//Add this rational number to the one passed as a parameter.
//A common denominator is found by multiplying the individual
//denominators.
//--------------------------------------------------------------------
public RationalNumber2 add(RationalNumber2 op2)
{
 int commonDenominator=denominator*op2.getDenominator();
 int numerator1=numerator*op2.getDenominator();
 int numerator2=denominator*op2.getNumerator();
 int sum=numerator1+numerator2;

 return new RationalNumber2(sum,commonDenominator);
}
//---------------------------------------------------------------
// Subtracts the rational number passed as a parameter from this 
// rational number.
//--------------------------------------------------------------
public RationalNumber2 subtract(RationalNumber2 op2)
 {
 int commonDenominator=denominator*op2.getDenominator();
 int numerator1=numerator*op2.getDenominator();
 int numerator2=denominator*op2.getNumerator();
 int difference=numerator1-numerator2;
 
 return new RationalNumber2(difference,commonDenominator);
}
//------------------------------------------------------
//Multiples this rational number by the one passed as a
//parameter.
//------------------------------------------------
public RationalNumber2 multiply(RationalNumber2 op2)
{
 int numer=numerator*op2.getNumerator();
 int denom=denominator*op2.getDenominator();

 return new RationalNumber2(numer,denom);
}
//--------------------------------------------------
//Divides this rational number by the one passed as a parameter
//by multiplying by the reciprocial of the secomd rational.
//--------------------------------------------------------
public RationalNumber2 divide(RationalNumber2 op2)
{
 return multiply(op2.reciprocal());
}
//-------------------------------------------------------------
//Derermines if this rational number is equal to the one passed
//as a parameter. Assumes they are both reduced.
//-------------------------------------------------------------
public boolean isLike(RationalNumber2 op2)
{
 return (numerator==op2.getNumerator() &&
          denominator==op2.getDenominator() );
}
//------------------------------------------------------
//Returns the rational number as a string.
//----------------------------------------------
public String toString()
{
  String result;
  if (numerator==0)
  result="0";
 else
   if (denominator==1)
   result=numerator+"";
  else
  result=numerator+"/"+denominator;
 return result;
}
//----------------------------------------------------------
//Reduces this rational number by dividing both the numerator
//and the denominator by their greast commom divisor.
//----------------------------------------------------------
private void reduce()
{
if (numerator !=0)
{
 int common=gcd(Math.abs(numerator),denominator);
 
 numerator=numerator/common;
 denominator=denominator/common;
}
}
//----------------------------------------------------------------
//Computes and returns the greatest common divisor of the two
//positive parameters. Uses Euclid's algorithm.
//------------------------------------------------------------------
private int gcd(int num1,int num2)
{
 while (num1 !=num2)
  if (num1>num2)
   num1=num1-num2;
  else 
   num2=num2-num1;

   return num1;
   }
//--------------------------------------------------------------------
// Compare two rational numbers ,the error accuracy is 0.0001.
//----------------------------------------------------------------------
public int compareTo(RationalNumber2 op2)
{
 double a,b,c;
   a=1.0*this.numerator/this.denominator;
  b=1.0*op2.getNumerator()/op2.getDenominator();
   c=a-b;

 if(c>0){
  if (c<0.0001)
  return 0;
  else
  return 1;}
 
  else
  if(c<0){
  if(c>=-0.0001)
   return 0;
   else
  return -1;}
 
  else
  return 0;
 }
}
  
