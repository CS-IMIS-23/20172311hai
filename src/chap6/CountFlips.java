/*
CountFlip.java            Author:zhaoxiaohai

使用Coin类，将硬币抛100次，输出朝上的次数
*/
public class CountFlips
{
  public static void main(String[] args)
 {
   final int LIMIT=100;
   int headnum=0;
   
  for(int a=1;a<=LIMIT;a++)
  {
   Coin myCoin=new Coin();
  
   myCoin.flip();
  
   if (myCoin.isHeads())
      headnum++;
    }
   System.out.println("朝上的次数是： "+headnum);
  }
}
     
