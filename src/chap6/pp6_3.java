/*
  pp6_3.java       Author:zhaoxiaohai
 
  输出一个12×12乘法表。
*/
public class pp6_3
{
  public static void main(String[] args)
  {
    final int MAX_NUM=12;
    for (int row=1;row<=MAX_NUM;row++)
    {
      for (int num=1;num<=row;num++)
         System.out.print(row+"×"+num+"="+row*num+" ");
      System.out.println();
     }
  }
}
