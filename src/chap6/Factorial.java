//***************************************************************
// Factorial.java          Author:zhaoxiaohai
//
// 用于计算输入的正整数n的阶乘。
//***************************************************************
import java.util.Scanner;

public class Factorial
{
  public static void main(String[]args)
  {
    int fac1=1,fac2=1;
    int n;
    int a=1;
    System.out.println("请您输入一个正整数，我将为您计算它的阶乘");
    Scanner scan=new Scanner(System.in);
    n=scan.nextInt();
    
    while(a<=n)
    {
      fac1 *=a; 
      a++;
    }
    System.out.println("while循环下 "+n+" 的阶乘为： "+fac1);
    
    for(int b=1;b<=n;b++)
      fac2 *=b;
    System.out.println("for循环下 "+n+" 的阶乘为： "+fac2);
  }
}
    
    
