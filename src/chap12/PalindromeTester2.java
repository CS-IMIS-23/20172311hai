package chap12;
/*
PalindromeTester2.java         Author:zahoxiaohai

实现第五章中PalindromeTester程序的递归版本
 */
import java.util.Scanner;

public class PalindromeTester2 {
    public static void main(String[] args) {
        String str, another="y";
        Scanner scan=new Scanner(System.in);

        while(another.equalsIgnoreCase("y")) {
            System.out.println("Enter a potential palindrome: ");
            str = scan.nextLine();
            if (Palindrome(str)==true)
                System.out.println("That string IS a palindrome.");
            else
                System.out.println("That string is NOT a palindrome.");

            System.out.println();
            System.out.println("Test another palindrome(y/n)?");
            another=scan.nextLine();
        }
    }
    /*
    public static boolean Palindrome(String str1){
        int left=0;
        int right=str1.length()-1;
        boolean result;

        while (str1.charAt(left) == str1.charAt(right) && left < right) {

                str1 = str1.substring(1, str1.length() - 1);
                Palindrome(str1);
            }

        if (left>=right)
            result=true;
        else
            result=false;

        return result;

    }
    */
    public static boolean Palindrome(String str){
        if(str.length()==1)
            return true ;
        else
        if(str.length()==2)
        {
            if(str.charAt(0)==str.charAt(str.length()-1))
                return true ;
            else
                return false ;
        }

        else
        if(str.charAt(0)==str.charAt(str.length()-1))
            return Palindrome(str.substring(1,str.length()-1)) ;
        else
            return  false;
    }
}


