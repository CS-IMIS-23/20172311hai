package chap12;

import java.util.Scanner;

public class Pascal {
    public static int digui(int i, int j) {
        if (j == 0 || j == i)
            return 1;
        else
            return digui(i - 1, j) + digui(i - 1, j - 1);

    }

    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.print("请输入杨辉三角的行数： ");
        int raw = scan.nextInt();

        int tri[][] = new int[raw][raw];
        for (int i = 0; i < raw; i++)
            for (int j = 0; j <= i; j++)
                tri[i][j] = digui(i, j);

        System.out.println("共有N行的杨辉三角为: ");
        for (int i = 0; i < raw; i++) {
            System.out.println();
            for (int n = raw - i; n >= 1; n--)
                System.out.print(" ");
            for (int j = 0; j <= i; j++)
                System.out.print(+tri[i][j] + " ");
        }
        System.out.println();

        System.out.println("共有N行的杨辉三角的第N行为： ");
            for (int j = 0; j < raw; j++)
                System.out.print(tri[(raw - 1)][j] + " ");
        }

    }



/*public class Pascal{
    public static int digui(int i,int j)
    {
        if(j==0||j==i)
            return 1;
        else
            return digui(i-1,j)+digui(i-1,j-1);

    }

    public static void main(String args[])
    {
        int tri[][];
        tri=new int[10][10];
        for(int i=0;i<10;i++)
            for(int j=0;j<=i;j++)
                tri[i][j]=digui(i,j);




            for(int j=0;j<10;j++)
                System.out.print(tri[9][j]+" ");
        }

    }
*/


