package cn.edu.besti.cs1723.Z2311;
import week10.LinkedHeap;
import week9.LinkedBinarySearchTree;

import java.util.Arrays;
import java.util.Iterator;

/*
 Sorting demonstrates sorting and searching on an array
*/
public class Sorting
{

    //希尔排序
    public static <T extends Comparable<T>>void ShellSort(T[] data)
    {
        for(int gap=data.length/2;gap>0;gap=gap/2)
        {
            for(int i=gap;i<data.length;i++)
            {
                int j = i;
                while(j-gap>=0 && data[j].compareTo(data[j-gap])<0)
                {
                    swap(data,j,j-gap);
                    j-=gap;
                }
            }
        }
    }

    //堆排序，利用小顶堆排序
    public static <T extends Comparable<T>>void HeapSort(T[] data)
    {
        LinkedHeap<T> linkedHeap = new LinkedHeap<>();
        for (int i=0;i<data.length;i++)
            linkedHeap.addElement(data[i]);
        for (int i=0;i<data.length;i++)
            data[i] = linkedHeap.removeMin();
    }

    //二叉树排序，利用二叉查找树排序
    public static <T extends Comparable<T>>void BinaryTreeSort(T[] data)
    {
        LinkedBinarySearchTree<T> linkedBinarySearchTree = new LinkedBinarySearchTree<>();
        for (int i=0;i<data.length;i++)
            linkedBinarySearchTree.addElement(data[i]);

        Iterator iterator = linkedBinarySearchTree.iteratorInOrder();
        int index = 0;
        while (iterator.hasNext())
        {
            data[index] = (T) iterator.next();
            index++;
        }
    }

    //选择排序，将数组排序
    public static <T extends Comparable<T>> void selectionSort1(T[] data) {
        String result = "";
        int min;

        for (int index = 0; index < data.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < data.length; scan++) {
                if (data[scan].compareTo(data[min]) < 0)
                    min = scan;
            }
            swap(data, min, index);
        }
    }
    //选择排序中用到的swap方法
    private static <T extends Comparable<T>> void swap(T[] data, int index1, int index2)
    {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    //选择排序，返回String结果
    public static <T extends Comparable<T>> String selectionSort(T[] data)
    {
        int min;
        T temp;

        for (int index = 0; index < data.length-1; index++)
        {
            min = index;
            for (int scan = index+1; scan < data.length; scan++)
                if (data[scan].compareTo(data[min])<0)
                    min = scan;

            swap(data, min, index);
        }
        return Arrays.toString(data);
    }


    //插入排序法
    public static <T extends Comparable<T>> void insertionSort(T[] data)
    {
        for (int index = 1; index < data.length; index++)
        {
            T key = data[index];
            int position = index;

            // shift larger values to the right
            while (position > 0 && data[position-1].compareTo(key) > 0)
            {
                data[position] = data[position-1];
                position--;
            }

            data[position] = key;
        }
    }


    //冒泡排序法
    public static <T extends Comparable<T>>  void bubbleSort(T[] data)
    {
        int position, scan;
        T temp;

        for (position =  data.length - 1; position >= 0; position--)
        {
            for (scan = 0; scan <= position - 1; scan++)
            {
                if (data[scan].compareTo(data[scan+1]) > 0)
                    swap(data, scan, scan + 1);
            }
        }
    }

    //归并排序法
    public static <T extends Comparable<T>> void mergeSort(T[] data)
    {
        mergeSort(data, 0, data.length - 1);
    }

    private static <T extends Comparable<T>> void mergeSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int mid = (min + max) / 2;
            mergeSort(data, min, mid);
            mergeSort(data, mid+1, max);
            merge(data, min, mid, max);
        }
    }

    //归并排序法里的merge方法
    @SuppressWarnings("unchecked")
    private static <T extends Comparable<T>> void merge(T[] data, int first, int mid, int last)
    {
        T[] temp = (T[])(new Comparable[data.length]);

        int first1 = first, last1 = mid;
        int first2 = mid+1, last2 = last;
        int index = first1;

        while (first1 <= last1 && first2 <= last2)
        {
            if (data[first1].compareTo(data[first2]) < 0)
            {
                temp[index] = data[first1];
                first1++;
            }
            else
            {
                temp[index] = data[first2];
                first2++;
            }
            index++;
        }

        while (first1 <= last1)
        {
            temp[index] = data[first1];
            first1++;
            index++;
        }

        while (first2 <= last2)
        {
            temp[index] = data[first2];
            first2++;
            index++;
        }

        for (index = first; index <= last; index++)
            data[index] = temp[index];
    }


    //快速排序法
    public static <T extends Comparable<T>> void quickSort(T[] data)
    {
        quickSort(data, 0, data.length - 1);
    }

    private static <T extends Comparable<T>> void quickSort(T[] data, int min, int max)
    {
        if (min < max)
        {
            int indexofpartition = partition(data, min, max);

            quickSort(data, min, indexofpartition - 1);

            quickSort(data, indexofpartition + 1, max);
        }
    }

    private static <T extends Comparable<T>> int partition(T[] data, int min, int max)
    {
        T partitionelement;
        int left, right;
        int middle = (min + max) / 2;

        partitionelement = data[middle];

        swap(data, middle, min);

        left = min;
        right = max;

        while (left < right)
        {

            while (left < right && data[left].compareTo(partitionelement) <= 0)
                left++;

            while (data[right].compareTo(partitionelement) > 0)
                right--;

            if (left < right)
                swap(data, left, right);
        }

        swap(data, min, right);

        return right;
    }

    //间隔排序法
    public static <T extends Comparable<T>> void IntervalSort(T[] array,int num) {
        for (int a =num;a>=1;a=a-2)
        {
            for (int position=array.length-1;position>=0;position--)
            {
                for (int scan = 0; scan < array.length - a; scan++)
                {
                    if (array[scan].compareTo(array[scan + a]) > 0)
                        swap(array, scan, scan + a);
                }
            }
        }
    }

}
