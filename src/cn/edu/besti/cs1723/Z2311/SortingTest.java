package cn.edu.besti.cs1723.Z2311;

public class SortingTest {
    public static void main(String[] args) {

        //希尔排序测试
        Integer[]a={23,2,1,4,6,3,56,2311,7};
        String result1="";
        Sorting.ShellSort(a);
        for (int i=0;i<a.length;i++){
            result1+=a[i]+" ";
        }
        System.out.println("a数组希尔排序结果："+result1);

        //堆排序测试
        Integer[]b={21,42,12,2,2311,5};
        String result2="";
        Sorting.HeapSort(b);
        for (int i=0;i<b.length;i++){
            result2+=b[i]+" ";
        }
        System.out.println("b数组堆排序结果："+result2);

        //二叉树排序测试
        Integer[]c={21,42,12,2,2311,5,6,123};
        String result3="";
        Sorting.BinaryTreeSort(c);
        for (int i=0;i<c.length;i++){
            result3+=c[i]+" ";
        }
        System.out.println("c数组二叉树排序结果："+result3);

        //选择排序测试
        Integer[]d={23,2,34,4,6,87,3,56,2311,1};
        String result4="";
        Sorting.selectionSort1(d);
        for (int i=0;i<d.length;i++){
            result4+=d[i]+" ";
        }
        System.out.println("d数组选择排序结果："+result4);

        //插入排序测试
        Integer[]e={23,2,4,6,87,3,2311,1};
        String result5="";
        Sorting.insertionSort(e);
        for (int i=0;i<e.length;i++){
            result5+=e[i]+" ";
        }
        System.out.println("e数组插入排序结果："+result5);

        //冒泡排序测试
        Integer[]f={23,2,4,3,6,87,3,2311,1};
        String result6="";
        Sorting.bubbleSort(f);
        for (int i=0;i<f.length;i++){
            result6+=f[i]+" ";
        }
        System.out.println("f数组冒泡排序结果："+result6);

        //归并排序测试
        Integer[]g={23,2,4,6,87,3,2311,1};
        String result7="";
        Sorting.mergeSort(g);
        for (int i=0;i<g.length;i++){
            result7+=g[i]+" ";
        }
        System.out.println("g数组归并排序结果："+result7);

        //快速排序测试
        Integer[]h={2,4,6,87,3,2311,1};
        String result8="";
        Sorting.quickSort(h);
        for (int i=0;i<h.length;i++){
            result8+=h[i]+" ";
        }
        System.out.println("h数组快速排序结果："+result8);

        //间隔排序测试
        Integer[]i={2,4,6,56,34,87,3,2311,1};
        String result9="";
        Sorting.IntervalSort(i,5);
        for (int n=0;n<i.length;n++){
            result9+=i[n]+" ";
        }
        System.out.println("i数组以5为间隔排序结果："+result9);

    }
}

