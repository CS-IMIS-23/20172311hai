package cn.edu.besti.cs1723.Z2311;
public class SearchingTest {
    public static void main(String[] args) {
        int []a={1,3,4,24,23,12,67};
        String array="";

        for (int i=0;i<a.length;i++)
            array+=a[i]+" ";

        System.out.println("数组中的元素为："+array);

        System.out.println("顺序查找是否有24（有的话返回索引值）："+Searching.SequenceSearch(a,2311,7));
        System.out.println("顺序查找是否有2（有的话返回索引值）："+Searching.SequenceSearch(a,2,7));

        System.out.println("二分查找（非递归）是否有24（有的话返回索引值）："+Searching.BinarySearch1(a,24,7));
        System.out.println("二分查找（非递归）是否有5（有的话返回索引值）："+Searching.SequenceSearch(a,5,7));

        System.out.println("二分查找（递归）是否有3（有的话返回索引值）："+Searching.BinarySearch2(a,3,0,6));

        System.out.println("插值查找是否有24："+Searching.InsertionSearch(a,24));


        System.out.println("斐波那契查找是否有67（有的话返回索引值）："+Searching.FibonacciSearch(a,7,67));
        System.out.println("斐波那契查找是否有6（有的话返回索引值）："+Searching.FibonacciSearch(a,7,6));

        System.out.println("树表查找是否有4："+Searching.TreeSearch(a,23));

        System.out.println("哈希查找是否有23（有的话返回索引值）:"+Searching.hashsearch(a,23));

        System.out.println("分块查找是否有23："+Searching.BlockSearch(a,23,new int[]{10,20,70}));
        System.out.println("分块查找是否有6："+Searching.BlockSearch(a,6,new int[]{10,20,70}));

    }
}
