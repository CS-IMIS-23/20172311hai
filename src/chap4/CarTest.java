//************************************************
// CarTest.java         Author:zhaoxiaohai
//
//  用来测试Car类 。
//************************************************
 public class CarTest
  {
    public static void main(String[] args)
   {
     Car cartest=new Car("baoma","730",1998);
     
     System.out.println("车的型号为： "+cartest.getCarsize());
     System.out.println("车的出场日期为： "+cartest.getCaryear());
    
     System.out.println("该车是否为古董： "+cartest.isAntique());
   
     System.out.println(cartest);
    }
}
