/*
Book.java         Author:zhaoxiaohai
编写一个Book类，其实例数据分别表示书名、作者、出版社以及版权日期。定义一个Book构造方法，接收并初始化实例数据；为所有实例数据定义获取和设置他们的方法；定义一个toString方法，返回几行描述该图书的字符串。创建一个Bookshelf驱动类，其main方法实例化并输出一些Book对象 。
*/
 public class Book{
     private String bookname;
     private String author;
     private String press;
     private String copyrightdate;
  
    public Book(String a,String b,String c,String d)  
     {
     bookname=a;
     author=b;
     press=c;
     copyrightdate=d;
     }
    public String getBookname()
    {return bookname;}
    public String getAuthor()
    { return author; }
     public String getPress()
    { return press;}
     public String getCopyrightdate()
    {return copyrightdate;}
    
    public void setBookname(String bo)
   {bookname=bo; }
    public void setAuthor(String au)
   {author=au;}    
    public void setPress(String pr)
   {press=pr;}
   public void setCopyrightdate(String co)
   {copyrightdate=co;   }    

    public String toString()
    {
    return bookname+"\t"+author+"\t"+press+"\t"+copyrightdate;
    }
}
