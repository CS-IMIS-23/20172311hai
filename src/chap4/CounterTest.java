//***************************************************************
// CounterTest.java       Author :zhaoxiaohai
//
//  用于测试Counter类的方法。
//**************************************************************
  public class CounterTest
  {
   public static void main(String[] args)
   {
    Counter a=new Counter();
    Counter b=new Counter();
    int c,d;
    
     c=a.click();
     System.out.println("c: "+c);
    
     a.click();
     a.click();
     b.click();
     System.out.println("a: "+a+" ,b: "+b.getCount());
     
     a.reset();
     b.reset();
    System.out.println("a: "+a.getCount()+" ,b: "+b);
     }
}
