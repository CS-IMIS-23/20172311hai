//***************************************************
// PairOfDice.java        Author:zhaoxiaohai
//
//  利用定义的Die类编写这个类，由两个Die对象组成 。
//***************************************************
  public class PairOfDice
  {
    private int facevalue1;
    private int facevalue2;
    private Die a,b;
        
   public PairOfDice()
  {
    a=new Die();
    b=new Die();   

   }
  
   public int Roll()
   {
   facevalue1=a.roll();
   facevalue2=b.roll();
    return facevalue1+facevalue2;
    }

    public void setFacevalue(int value1,int value2)
   {
   facevalue1=value1;
   facevalue2=value2;
   }
   
   public int getFacevalue1()
   {
   return facevalue1;
   }
    public int getFacevalue2()
   {
   return facevalue2;
   }

   
   public String toString()
   {
   String result =Integer.toString(facevalue1+facevalue2);
   return result;
    }
}
