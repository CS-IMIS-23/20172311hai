//*****************************************************************
//  Lights.java           Author:zhaoxiaohai
//
//  用于测试Bulb类。
//******************************************************************
   public class Lights
   {
   public static void main(String[] args)
   {
     Bulb bright;
     int a;
     bright=new Bulb();
     
     a= bright.getBrightness();
     System.out.println("灯泡初始亮度： "+a);
    
      bright.setBrightness(12);
     System.out.println("调节亮度为： "+bright.getBrightness());
   
     System.out.println("灯泡是否开启： "+bright.ifopen());
     }
}
