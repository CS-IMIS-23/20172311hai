//********************************************************
//  Counter.java     Author :zhaoxiaohai
//
//  编写一个Counter类，表示一个检录计数器，用于统计进入房间的人数
//*******************************************************
  
 public class Counter
  {
    private int a;
   
    public Counter()
    {
     a=0;
    }
   
    public int click()
   {
    a=a+1;
    return a;
    }
    
    public int getCount()
    {
    return a;
   }
  
    public int reset()
   {
    a=0;
    return a;
    }
   
    public String toString()
   {
    String result=Integer.toString(a);
    return result;
    }
}

