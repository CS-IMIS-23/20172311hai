//***********************************************
// Car.java           Author :zhaoxiaohai
//  
//创建一个Car类，并创建其一些方法 。
//***********************************************
 public class Car
 {
  public String carmade;
  public String carsize;
  public int caryear;
  public boolean old;
  
  public Car(String made,String size,int year)
 {
   carmade=made;
   carsize=size;
   caryear=year;
 }
   
  public String getCarmade()
  {
    return carmade;
  }
  public String getCarsize()
  {
    return carsize;
  } 
  public int getCaryear()
  { 
     return caryear;
  }

   public boolean isAntique()
  {
   old=2018-caryear>45;
    return old;
  }
 
  public String toString()
  {
  return carmade+"\t"+ carsize+"\t"+caryear;
  }
  public Boolean toBoolean()
  {
   return old;
  }
}


