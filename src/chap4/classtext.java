//**************************************************************
//classtext.java        Author:zhaoxiaohai
//用于生成一个-10～10的随机数，并且生成其对应的16进制数。
//*************************************************************

import java.util.Random;
public class classtext {
    public static void main(String[] args) {
        Random generate =new Random();
        float PseudoNumber=generate.nextFloat()*20-10;
        System.out.println("PseudoNumber: "+PseudoNumber);
        System.out.println("PseudoNumber的16进制数: "+ Float.toHexString(PseudoNumber));
    }
}
