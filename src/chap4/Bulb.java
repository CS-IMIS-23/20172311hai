//********************************************************************
//  Bulb.java            Author :zhaoxiaohai
//
//  编写一个Bulb类，该类代表一个可以开或关的灯泡。
//******************************************************************
   public class Bulb
   {
   boolean state;//灯的当前状态，false为关，true为开。
   
  public int brightness;//灯的亮度，数值为0～100。
   
   /*初始化灯为关闭状态，亮度为0*/
    public Bulb()
    {
      brightness=0;
   }
   
    /*调节亮度0~100*/
   public void setBrightness(int a)
  {
   brightness=a;
   }
   
   /*获取灯泡亮度*/
   public int getBrightness()
   {
    return brightness;
   }
   /*获取灯泡状态，开或者关。*/
   public boolean ifopen()
  {
   state=brightness-0>0;
   return state;
   }
  
   public String toString()
   {
   String result= Integer.toString(brightness);
   return result;  
   }
   public Boolean toBoolean()
   {
    return state;
   }
}
