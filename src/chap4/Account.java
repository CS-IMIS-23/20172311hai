package chap4;//****************************************************************
//  Account.java         Author :zhaoxiaohai
//  
//  Represents a bank account with basic services such as deposit 
//  and withdraw.
//****************************************************************
   import java.text.NumberFormat;
   
   public class Account{ 
   private double RATE=0.035;//interest rate of 3.5%

   private long acctNumber;
   private double balance;
   private String name;
   //---------------------------------------------------
   // Sets up the account by defining its owner, account number,
   // and initial balance.
   //-----------------------------------------------------  
   public Account(String owner,long account,double initial)
  {
   name=owner;
   acctNumber = account;
   balance=initial;
   }
   //----------------------------------------------------------
   //  Deposits the specified amount into the account.Returns 
   //  the new balance.
   //-----------------------------------------------------------
    public double deposit(double amount)
    {
     balance=balance+amount;
     return balance;
     }
    //----------------------------------------------------
    // Withdraws the specified amount from the account and 
    // applies the fee. Return the new balance. 
    //----------------------------------------------------
     public double withdraw(double amount,double fee)
     {
         if(balance>=amount+fee)
             balance=balance-amount-fee;
         else
             System.out.println("您的余额不足，无法完成交易,您的余额是："+balance);
         return balance;
     }
       /*
       使利息增加3%
        */
       public double addRate()
       {
           RATE=RATE+0.03;
           return RATE;
       }
    //---------------------------------------------------------
    // Adds interest to the account and returns the new balance.
    //---------------------------------------------------------
    public double addInterest()
    {
    balance=balance+(balance*RATE);
    return balance;
    }
    //--------------------------------------------------------
    // Returns the current balance of the account. 
    //-----------------------------------------------------
     public double getBalance()
    {
     return balance;
     }
    //-------------------------------------------------------
    // Returns one-line description of the account as a string.
    //-------------------------------------------------------
     public String toString()
    {
    NumberFormat fmt=NumberFormat.getCurrencyInstance();
    return acctNumber+"\t"+name+"\t"+fmt.format(balance);
   }
   }
  
