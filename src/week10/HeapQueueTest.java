package week10;

//pp12.1，用堆实现的队列类的测试类

public class HeapQueueTest {
    public static void main(String[] args) {
        HeapQueue heapQueue=new HeapQueue();

        int []array={1,2,3,4,5,6,7,8,9};

        for (int a=0;a<array.length;a++)
        {
            heapQueue.addElement1(array[a]);
        }

        for (int n=0;n<array.length;n++){
            System.out.println(heapQueue.removeNext());
        }
    }
}
