package week10;

//pp12.1,用堆实现的队列的节点类
public class HeapQueueNode<T> implements Comparable<HeapQueueNode>
{
    private static int nextOrder = 0;
    private int arrivalOrder;
    private T element;


    //Creates a new queue
    public HeapQueueNode(T element)
    {
        this.element = element;
        arrivalOrder = nextOrder;
        nextOrder++;
    }

    //Returns the element in this node.
    public T getElement()
    {
        return element;
    }

    //Returns the arrival order for this node.
    public int getArrivalOrder()
    {
        return arrivalOrder;
    }


    //Returns a string representation for this node.
    @Override
    public String toString()
    {
        return (element + "  " + arrivalOrder);
    }

    //Returns 1 if the this object has higher priority than
    //the given object and -1 otherwise.
    @Override
    public int compareTo(HeapQueueNode obj)
    {
        int result;

        if (arrivalOrder > obj.getArrivalOrder())
            result = 1;
        else
            result = -1;

        return result;
    }
}
