package week10;

import java.util.Iterator;

/*
  LinkedHeapTest sorts a given array of Comparable objects using a heap.
 */
public class LinkedHeapTest
{
    public static void main(String[] args) {

        LinkedHeap<Integer> temp=new LinkedHeap<Integer>();
        int[]data={36,30,18,40,32,45,22,50};

        //将数组中的元素添加到小顶堆中
        for (int i = 0; i < data.length; i++)
            temp.addElement(data[i]);

        //层序输出输出构造好的小顶堆序列
        String result="";
        Iterator iterator =temp.iteratorLevelOrder();//使用层序遍历迭代器
        while(iterator.hasNext()){
            result+=iterator.next()+" ";
        }
        System.out.println("层序输出输出构造好的小顶堆序列结果：");
        System.out.println(result);

        //输出每轮排序的结果（数组的结果）
        System.out.println("输出每轮排序的结果（链表的结果）:");
        for (int a=0;a<data.length;a++)
        {
            data[a]=temp.removeMin();
            String result1="";
            for (int b=0;b<=a;b++){
                result1+=data[b]+" ";
            }
            Iterator iterator1=temp.iteratorLevelOrder();
            while (iterator1.hasNext()){
                result1+=iterator1.next()+" ";
            }
            System.out.println("第"+(a+1)+"次："+result1);

        }

        String result2="";
        for (int a=0;a<data.length;a++){
            result2+=data[a]+" ";
        }
        System.out.println("排序结果："+result2);
    }
}
