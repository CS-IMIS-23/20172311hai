package week10;

//pp12.1,用堆实现的队列类
public class HeapQueue<T> extends SmallArrayHeap<HeapQueueNode<T>>
{

    //Creates an empty queue.
    public HeapQueue()
    {
        super();
    }


    //Adds the given element to this Queue.
    public void addElement1(T object)
    {
        HeapQueueNode<T> obj = new HeapQueueNode<>(object);
        super.addElement(obj);
    }


    //Removes the next highest priority element from this priority
    //queue and returns a reference to it.
    public T removeNext()
    {
        HeapQueueNode<T> obj = (HeapQueueNode<T>)super.removeMin();
        return obj.getElement();
    }
}

