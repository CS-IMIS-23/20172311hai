package week10;


//PriorityQueue implements a priority queue using a heap.

public class PriorityQueue<T> extends SmallArrayHeap<PrioritizedObject<T>>
{

    //Creates an empty priority queue.
    public PriorityQueue()
    {
        super();
    }


    //Adds the given element to this PriorityQueue.
    public void addElement(T object, int priority)
    {
        PrioritizedObject<T> obj = new PrioritizedObject<T>(object, priority);
        super.addElement(obj);
    }


    //Removes the next highest priority element from this priority
    //queue and returns a reference to it.
    public T removeNext()
    {
        PrioritizedObject<T> obj = (PrioritizedObject<T>)super.removeMin();
        return obj.getElement();
    }
}
