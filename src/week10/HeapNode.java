package week10;

import week8.BinaryTreeNode;

 //HeapNode represents a binary tree node with a parent pointer for use
 //in heaps.

public class HeapNode<T> extends BinaryTreeNode<T>
{
    protected HeapNode<T> parent;


    //Creates a new heap node with the specified data.
    public HeapNode(T obj)
    {
        super(obj);
        parent = null;
    }


    //Return the parent of this node.
    public HeapNode<T> getParent()
    {
        return parent;
    }

   //Sets the element stored at this node.
    @Override
    public void setElement(T obj)
    {
        element = obj;
    }

    //Sets the parent of this node.
    public void setParent(HeapNode<T> node)
    {
        parent = node;
    }
}


