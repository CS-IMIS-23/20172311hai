package week10;

//HeapSort sorts a given array of Comparable objects using a heap.

public class SmallHeapSort<T>
{
    //Sorts the specified array using a SmallHeap

    public void HeapSort(T[] data)
    {
        SmallArrayHeap<T> temp = new SmallArrayHeap<T>();

        // copy the array into a heap
        for (int i = 0; i < data.length; i++)
            temp.addElement(data[i]);

        // place the sorted elements back into the array
        int count = 0;
        while (!(temp.isEmpty()))
        {
            data[count] = temp.removeMin();
            count++;
        }
    }
}

