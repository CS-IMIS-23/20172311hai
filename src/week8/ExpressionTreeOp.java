package week8;

/*
 表达式树中的元素类
*/
public class ExpressionTreeOp {

    private int termType;
    private char operator;
    private int value;

    public ExpressionTreeOp() {

    }
    //3个参数的构造函数的
    public ExpressionTreeOp(int termType, char operator, int value) {
        super();
        this.termType = termType;
        this.operator = operator;
        this.value = value;
    }

    //判断是不是操作符号
    public boolean isOperator() {
        return (termType==1);

    }

    public char getOperator() {
        return operator;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        if(termType==1){
            return operator+"";

        }
        else{
            return value+"";
        }
    }


}