package week8;
import week2.EmptyCollectionException;
import week5.ArrayUnorderedList;
import week5.ElementNotFoundException;
import week5.UnorderedListADT;
import java.util.*;

//链式二叉树类的实现
public class LinkedBinaryTree<T> implements Iterable<T>, BinaryTreeADT<T> {

    protected BinaryTreeNode<T> root;
    protected int modCount;
    private LinkedBinaryTree<T> left, right;

    //创建一个空的二叉树
    public LinkedBinaryTree() {
        root = null;
    }

    //创建一个只有根结点的二叉树
    public LinkedBinaryTree(T element) {
        root = new BinaryTreeNode<T>(element);
    }

    //为该节点添加左右孩子结点
    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right) {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
        this.left = left;
        this.right = right;
    }

    //返回根结点
    public BinaryTreeNode<T> getRootNode() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("BinaryTreeNode ");
        }
        return root;
    }
    //返回结点的左侧子结点
    public LinkedBinaryTree<T> getLeft() {
        return left;
    }
    //返回结点的右侧子结点
    public LinkedBinaryTree<T> getRight() {
        return right;
    }

    //返回树的高度
    public int getHeight() {
        BinaryTreeNode temp = root;
        int height = 1;
        if(root == null )
            return 0;
        if (root != null){
            height++;
        }
        temp = temp.left;
        while (temp.IfInternalNode() != true){
            height++;
            temp = temp.left;
        }
        height++;
        return height;
    }

    //返回根结点的元素
    @Override
    public T getRootElement() throws EmptyCollectionException {
        if (root.getElement().equals(null)) {
            throw new EmptyCollectionException("BinaryTreeNode ");
        }
        return root.getElement();
    }

    //判断树是否为空
    @Override
    public boolean isEmpty() {
        return (root == null);
    }

    //返回树中元素的总数
    @Override
    public int size() {
        int size = 0;
        if(root.getLeft()!=null)
            size+=1;
        if(root.getRight()!=null)
            size+=1;
        return size;
    }

    @Override
    //判断是否包含某个元素
    public boolean contains(T targetElement) {
        if(targetElement == find(targetElement))
            return true;
        else
            return false;
    }

   //返回找到的元素，若找不到则抛出异常信息
    public T find(T targetElement) {
        BinaryTreeNode<T> current = findNode(targetElement, root);

        if (current == null)
            throw new ElementNotFoundException("LinkedBinaryTree");

        return (current.getElement());
    }

    private BinaryTreeNode<T> findNode(T targetElement, BinaryTreeNode<T> next) {
        if (next == null)
            return null;

        if (next.getElement().equals(targetElement))
            return next;

        BinaryTreeNode<T> temp = findNode(targetElement, next.getLeft());

        if (temp == null)
            temp = findNode(targetElement, next.getRight());

        return temp;
    }

    //查找指定元素并且返回该结点以方便添加孩子
    public BinaryTreeNode<T> findNode(T targetElement) {
        BinaryTreeNode<T> current = findNode(targetElement, root);

        if (current == null)
            throw new ElementNotFoundException("LinkedBinaryTree");

        return current;
    }

    //删除二叉树某结点的右侧部分
    public void removeRightSubtree(T node){
        BinaryTreeNode<T> next = new BinaryTreeNode<T>(node);
        if(root == null)
            throw new EmptyCollectionException("Tree is Empty!!");
        if((T) next.getRight() == null)
            System.out.println("此处没有右枝杈！！");
        else
            next.setRight(null);
    }

    //删除二叉树中全部元素
    public void removeElement(){
        BinaryTreeNode<T> root = null;
        this.root = root;
    }

    @Override
    //为树的中序遍历返回一个迭代器
    public Iterator<T> iteratorInOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        inOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }
    protected void inOrder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList) {
        if (node != null) {
            inOrder(node.getLeft(), tempList);
            tempList.addToRear(node.getElement());
            inOrder(node.getRight(), tempList);
        }
    }

    @Override
    //为树的前序遍历返回一个迭代器
    public Iterator<T> iteratorPreOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }
    private void preOrder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList) {
        if (node != null) {
            tempList.addToRear(node.getElement());
            inOrder(node.getLeft(), tempList);
            inOrder(node.getRight(), tempList);
        }
    }

    @Override
    //为树的后序遍历返回一个迭代器
    public Iterator<T> iteratorPostOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        postOrder(root, tempList);
        return new TreeIterator(tempList.iterator());
    }
    private void postOrder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList) {
        if (node != null) {
            tempList.addToRear(node.getElement());
            inOrder(node.getLeft(), tempList);
            inOrder(node.getRight(), tempList);
        }
    }

    @Override
    //为树的层序遍历返回一个迭代器
    public Iterator<T> iteratorLevelOrder() {
        ArrayUnorderedList<BinaryTreeNode<T>> nodes = new ArrayUnorderedList<BinaryTreeNode<T>>();
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        BinaryTreeNode<T> current;

        nodes.addToRear(root);

        while (!nodes.isEmpty()) {
            current = nodes.removeFirst();

            if (current != null) {
                tempList.addToRear(current.getElement());
                if (current.getLeft() != null)
                    nodes.addToRear(current.getLeft());
                if (current.getRight() != null)
                    nodes.addToRear(current.getRight());
            } else
                tempList.addToRear(null);
        }

        return new TreeIterator(tempList.iterator());
    }

    @Override
    public Iterator<T> iterator() {
        return iteratorInOrder();
    }

    //中序遍历输出
    public void toInString(){
        inOrder(root);
    }
    private void inOrder(BinaryTreeNode root) {
        if (null != root) {
            inOrder(root.getLeft());
            System.out.print(root.getElement() + "\t");
            inOrder(root.getRight());
        }
    }

    //前序遍历输出
    public void toPreString(){
        preOrder(root);
    }
    private void preOrder(BinaryTreeNode root){
        if(null!= root){
            System.out.print(root.getElement() + "\t");
            preOrder(root.getLeft());
            preOrder(root.getRight());
        }
    }

    //后序遍历输出
    public void toPostString(){
        postOrder(root);
    }
    private void postOrder(BinaryTreeNode root) {
        if (null != root) {
            postOrder(root.getLeft());
            postOrder(root.getRight());
            System.out.print(root.getElement() + "\t");
        }
    }

    //递归方法实现层序输出
    public void toLevelString1(){
        if(root == null)
            return;
        int height = getHeight()+1;
        for(int i = 1; i <= height; i++){
            levelOrder(root,i);
        }
    }
    private void levelOrder(BinaryTreeNode root,int level){
        if(root == null || level < 1){
            return;
        }
        if(level == 1){
            System.out.print(root.getElement() + "\n");
            return;
        }
        levelOrder(root.getLeft(),level - 1);
        levelOrder(root.getRight(),level - 1);
    }
    //非递归方法实现层序输出
    public void toLevelString2(){
        BinaryTreeNode temp;
        Queue<BinaryTreeNode> queue=new LinkedList<BinaryTreeNode>();
        queue.offer(root);
        while(!queue.isEmpty()){
            temp=queue.poll();
            System.out.print(temp.getElement()+"\n");
            if(null!=temp.getLeft())
                queue.offer(temp.getLeft());
            if(null!=temp.getRight()){
                queue.offer(temp.getRight());
            }
        }
     }

    //打印树
    @Override
    public String toString(){
        UnorderedListADT<BinaryTreeNode<T>> nodes = new ArrayUnorderedList<BinaryTreeNode<T>>();
        UnorderedListADT<Integer> levelList = new ArrayUnorderedList<Integer>();

        BinaryTreeNode<T> current;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + "  ";
            } else {
                for (int i = 0; i < ((Math.pow(2,
                        (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + "  ";
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + "  ";
            }
        }
        return result;
    }

    //内部类，表示该树元素上的迭代器
    private class TreeIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;

        public TreeIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        public T next() throws NoSuchElementException {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}