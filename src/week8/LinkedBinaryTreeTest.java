package week8;

public class LinkedBinaryTreeTest {
    public static void main(String args[]){
        LinkedBinaryTree stringLinkedBinaryTree1 = new LinkedBinaryTree<>("我是赵晓海");
        LinkedBinaryTree stringLinkedBinaryTree2 = new LinkedBinaryTree<>("我20周岁了");
        LinkedBinaryTree stringLinkedBinaryTree3 = new LinkedBinaryTree<>("我爱好广泛");
        LinkedBinaryTree stringLinkedBinaryTree4 = new LinkedBinaryTree<>("IG牛逼");

        LinkedBinaryTree node1 = new LinkedBinaryTree("你好",stringLinkedBinaryTree1, stringLinkedBinaryTree2);
        LinkedBinaryTree node2 = new LinkedBinaryTree("我来自山东",stringLinkedBinaryTree3,node1);
        LinkedBinaryTree node3 = new LinkedBinaryTree("树的根",node2,stringLinkedBinaryTree4);

        //测试contains方法，测试树中是否有String型的你好，应该返回true
        System.out.println("书中是否有String型的你好:"+node3.contains("你好"));

        //测试getRight方法，返回并打印node2节点的右子树
        System.out.println("返回并打印node2节点的右子树:");
        System.out.println(node2.getRight().toString());

        //测试toString方法，打印以node3为根节点的树
        System.out.println("以node3为根节点的树:");
        System.out.println(node3.toString());

        //测试前序输出方法，打印以node3为根节点的树的前序输出
        System.out.print("前序输出：");
        node3.toPreString();

        //测试后序输出方法，打印以node3为根节点的树的后序输出
        System.out.println("\n后序输出：");
        node3.toPostString();
    }
}