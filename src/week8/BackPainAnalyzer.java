package week8;

import java.io.FileNotFoundException;

/*
 背部疼痛诊断器
 */
public class BackPainAnalyzer {


     // Asks questions of the user to diagnose a medical problem.

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("让我们做一个小测试");
        DecisionTree expert = new DecisionTree("C:\\Users\\user\\IdeaProjects\\socialsea\\socialsea\\src\\shiyan2\\input");
        expert.evaluate();

        System.out.println("打印决策树如下：");
        System.out.println(expert.getTree().toString());

//        System.out.println("树中叶节点个数"+expert.CountLeaf(expert.getTree().getRootNode()));
//
//        LinkedBinaryTree linkedBinaryTree=expert.getTree();
//        System.out.println("树的深度为"+linkedBinaryTree.getHeight());
//
//        System.out.println("递归方法层序遍历输出：");
//        expert.toLevelString1();
//        System.out.println("\n非递归方法层序遍历输出：");
//        expert.toLevelString2();
    }
}
