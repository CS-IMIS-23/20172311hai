package week8;

import java.util.Iterator;

/*
 二叉树的ADT
*/
public interface BinaryTreeADT<T> {

    // 获取根部元素
    public T getRootElement();

    //判断是否是空树
    public boolean isEmpty();

    // 返回树中元素的个数
    public int size();

    //判定二叉树中是否存在当前的元素
    public boolean contains(T targetElement);

    //在二叉树中查找响应的元素
    public T find(T targetElement);

    //输出二叉树
    @Override
    public String toString();

    //返回二叉树的迭代器
    public Iterator<T> iterator();

    //Returns an iterator that represents an inorder traversal on this binary tree.
    public Iterator<T> iteratorInOrder();

    //Returns an iterator that represents a preorder traversal on this binary tree.
    public Iterator<T> iteratorPreOrder();

    //Returns an iterator that represents a postorder traversal on this binary tree.
    public Iterator<T> iteratorPostOrder();

    //Returns an iterator that represents a levelorder traversal on the binary tree.
    public Iterator<T> iteratorLevelOrder();

}