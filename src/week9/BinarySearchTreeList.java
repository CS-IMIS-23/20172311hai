package week9;

import week5.ListADT;
import week5.OrderedListADT;

import java.util.Iterator;


 //BinarySearchTreeList represents an ordered list implemented using a binarysearch tree.

 public class BinarySearchTreeList<T> extends LinkedBinarySearchTree<T> implements ListADT<T>, OrderedListADT<T>, Iterable<T>
{

    //Creates an empty BinarySearchTreeList.

    public BinarySearchTreeList()
    {
        super();
    }


     //Adds the given element to this list.

    @Override
    public void add(T element)
    {
         addElement(element);
    }


    //Removes and returns the first element from this list.

    @Override
    public T removeFirst()
    {
        return removeMin();
    }


    //Removes and returns the last element from this list.

    @Override
    public T removeLast()
    {
        return removeMax();
    }


     //Removes and returns the specified element from this list.

    @Override
    public T remove(T element)
    {
        return removeElement(element);
    }


     //Returns a reference to the first element on this list.

    @Override
    public T first()
    {
        return findMin();
    }


    //Returns a reference to the last element on this list.

    @Override
    public T last()
    {
        return findMax();
    }


     //Returns an iterator for the list.

    @Override
    public Iterator<T> iterator()
    {
        return iteratorInOrder();
    }
}

