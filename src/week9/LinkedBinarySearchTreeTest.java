package week9;

public class LinkedBinarySearchTreeTest {
    public static void main(String[] args) {
        LinkedBinarySearchTree linkedBinarySearchTree=new LinkedBinarySearchTree();
        linkedBinarySearchTree.addElement(6);
        linkedBinarySearchTree.addElement(2);
        linkedBinarySearchTree.addElement(3);
        linkedBinarySearchTree.addElement(7);
        linkedBinarySearchTree.addElement(9);
        linkedBinarySearchTree.addElement(5);
        linkedBinarySearchTree.addElement(6);

        //构造的二叉查找树
        System.out.println("构造的二叉查找树："+"\n"+linkedBinarySearchTree.toString());
        //删除并输出最大值
        System.out.println("删除的最大值："+linkedBinarySearchTree.removeMax());
        System.out.println("当前二叉查找树：\n"+linkedBinarySearchTree.toString());

        linkedBinarySearchTree.removeAllOccurrences(6);
        System.out.println("删除全部6之后的二叉查找树：\n"+linkedBinarySearchTree.toString());

        System.out.println("当前树中的最小值:"+linkedBinarySearchTree.findMin());
        System.out.println("当前树中的最大值:"+linkedBinarySearchTree.findMax());

    }
}
