package week9;

//AVL树类的测试类
public class AVLTreeTest {
    public static void main(String[] args) {
        AVLTree avlTree = new AVLTree(7);
        avlTree.addElement(5);
        avlTree.addElement(6);
        avlTree.addElement(3);
        avlTree.addElement(4);
        avlTree.addElement(1);
        avlTree.addElement(9);
        avlTree.addElement(8);
        avlTree.addElement(10);
        avlTree.addElement(11);

        System.out.println("\n打印当前AVL树：" + avlTree.toString());

        System.out.println("AVL树的高度：" + avlTree.height());
        System.out.println("AVL树最小值：" + avlTree.findMin(avlTree.root));
        System.out.println("AVL树最大值：" + avlTree.findMax(avlTree.root));

        System.out.print("\n前序输出： ");
        avlTree.toPreString();

        System.out.print("\n中序输出： ");
        avlTree.toInString();

        System.out.print("\n后序输出： ");
        avlTree.toPostString();

        System.out.print("\n层序输出： ");
        avlTree.toLevelString();

        System.out.println("\n删除结点7");
        avlTree.removeElement(7);
        System.out.println("\n打印当前AVL树：" + avlTree.toString());
    }
}