package week9;

public class BinarySearchTreeListTest {
    public static void main(String[] args) {
        BinarySearchTreeList binarySearchTreeList=new BinarySearchTreeList();
        binarySearchTreeList.add(6);
        binarySearchTreeList.add(5);
        binarySearchTreeList.add(4);
        binarySearchTreeList.add(3);
        binarySearchTreeList.add(1);
        binarySearchTreeList.add(9);
        binarySearchTreeList.add(7);
        binarySearchTreeList.add(8);
        binarySearchTreeList.add(5);

        System.out.println(binarySearchTreeList.toString());

        binarySearchTreeList.removeLast();
        System.out.println(binarySearchTreeList.toString());

        binarySearchTreeList.removeAllOccurrences(5);
        System.out.println(binarySearchTreeList.toString());
    }
}
