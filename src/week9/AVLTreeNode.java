package week9;

//AVL树的节点类
public class AVLTreeNode<T extends Comparable<T>> {
    protected T elem;
    protected AVLTreeNode<T> left, right;
    protected int height;

    //构造函数1
    public AVLTreeNode(T elem) {
        this.elem = elem;
        this.left = null;
        this.right = null;
        this.height = 0;
    }
    //构造函数2
    public AVLTreeNode(T elem, AVLTreeNode<T> left, AVLTreeNode<T> right) {
        this.elem = elem;
        this.left = left;
        this.right = right;
        this.height = 0;
    }
    public T getElem() {
        return elem;
    }
    public AVLTreeNode<T> getRight() {
        return right;
    }
    public AVLTreeNode<T> getLeft() {
        return left;
    }

    public void setElem(T elem) {
        this.elem = elem;
    }

    public void setLeft(AVLTreeNode<T> left) {
        this.left = left;
    }

    public void setRight(AVLTreeNode<T> right) {
        this.right = right;
    }

    //判断是否是内部结点
    public boolean internalNode(){
        if(left == null && right == null)
            //叶结点的左侧和右侧均无结点
            return false;
        else
            //内部节点的左侧或右侧都可以有结点
            return true;
    }
}