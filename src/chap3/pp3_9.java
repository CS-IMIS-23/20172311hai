
/*
  pp3_9.program                作者：赵晓海
  随机生成圆柱体底面半径和高度(在区间[1,20]之间)，求其表面积和体积。
 */
import java.util.Random;
public class pp3_9 {
    public static void main(String[] args) {
        Random generator=new Random();

        int r,h;
        double s,v;

        r=generator.nextInt(20)+1;
        h=generator.nextInt(20)+1;

        s=2*Math.PI*r*h+2*Math.PI*Math.pow(r,2);
        v=Math.PI*Math.pow(r,2)*h;

        System.out.println("以"+r+"为半径，以"+h+"为高的圆柱体的表面积为： "+s+" ,体积为： "+v);
    }
}
