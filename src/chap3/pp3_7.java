
 /*
  pp3_7.java    Author :zhaoxiaohai
   已知三角形三边长，计算三角形面积。
 */
import java.util.Scanner;
import java.text.DecimalFormat;
public class pp3_7 {
    public static void main(String[] args) {
        double a,b,c,s,S;

        Scanner scan =new Scanner(System.in);
        System.out.println("请您分别输入三角形的三条边长：");
        a=scan.nextDouble();
        b=scan.nextDouble();
        c=scan.nextDouble();
        s=(a+b+c)/2;
        S=Math.sqrt((s-a)*(s-b)*(s-c)*s);
        DecimalFormat fmt=new DecimalFormat("0.###") ;

        System.out.println("以"+a+","+b+","+c+"为边长的三角形的面积为： "+fmt.format(S));

    }
}
