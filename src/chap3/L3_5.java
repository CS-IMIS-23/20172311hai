//***************************************************************************************
//CircleStats.java            Author:zhaoxiaohai
//
//Demonstrate the formatting of decimal values using the 
//DecimalFormat class.
//**************************************************************************************

  import java.util.Scanner;
  import java.text.DecimalFormat;

  public class L3_5
   {
   //-------------------------------------------------------------------
   // Calculates the area and circumference of a circle given its radius
   //-------------------------------------------------------------------
   public static void main(String [] args)
   {
      int radius;
      double area,circumference;
     
      Scanner scan=new Scanner(System.in);

      System.out.println("Enter the circle's radius: ");
      radius=scan.nextInt();
      
      area= Math.PI*Math.pow(radius,2);
      circumference=2*Math.PI*radius;
    
      //Round the output to there decimal places
      DecimalFormat fmt=new DecimalFormat("0.###");
      
      System.out.println("The circle's area: "+fmt.format(area));
      System.out.println("The circle's circumference: "+
                          fmt.format(circumference));

    }

}
