/*
    pp3_2.java   Author:zhaoxiaohai
  
  输出两个整数的立方值之和。
*/
   import java.util.Scanner;
   public class pp3_2
   {
   public static void main(String [] args)
   {
    int a,b,c,d,e;
   
    Scanner scan =new Scanner(System.in);
    System.out.println("请您输入第一个整数：");
     a=scan.nextInt();
    System.out.println("请您输入第二个整数：");
     b=scan.nextInt();
    c=(int)Math.pow(a,3);
    d=(int)Math.pow(b,3);
    e=c+d;
    System.out.println(a+"^3+"+b+"^3= "+e);
   }
}
   
