
/*
 pp3_6.java       Author:zhaoxiaohai
 输入球体半径，输出其体积和表面积。
 */
import java.util.Scanner;
import java.text.DecimalFormat;
public class pp3_6 {
    public static void main(String[] args) {
        double r,v,s;

        Scanner scan=new Scanner(System.in) ;
        System.out.println("请您输入一个值作为球体的半径：");
        r=scan.nextDouble();
        s=Math.PI*4*Math.pow(r,2);
        v=4/3*Math.PI*Math.pow(r,3);

        DecimalFormat fmt=new DecimalFormat("0.###");

        System.out.println("以"+r+"为半径的球体的表面积为："+fmt.format(s)+" ,体积为："+fmt.format(v));
    }
}
