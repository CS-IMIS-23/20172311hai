
/*
   pp3_4.java     Author:zhaoxiaohai
   输入一个小数，分别表示不大于它的最大整数和不小于它的最小整数。
 */
  import java.util.Scanner;
public class pp3_4 {
    public static void main(String[] args) {
        double a;
        int b,c;

        Scanner scan =new Scanner(System.in);
        System.out.println("请您输入一个小数：");
        a=scan.nextDouble();
        b=(int)Math.floor(a);
        c=(int)Math.ceil(a);

        System.out.println("不大于"+a+"的最大整数为： "+b+", 不小于"+a+"的最小整数为： "+c);
    }
}
