//*************************************************************************
//Calculate Distance.java              作者：赵晓海
//
// 计算两点之间的距离。
//*************************************************************************

  import java.util.Scanner;
  
  public class pp3_5
   {
   public static void main(String [] args)
   {
    double a,b,c,d,e,f,g,h,i,j;
   
   Scanner scan= new Scanner(System.in);
   
   System.out.println("请您输入第一个点的横坐标： ");
   a=scan.nextDouble();

   System.out.println("请您输入第一个点的纵坐标： ");
   b=scan.nextDouble();

   System.out.println("请您输入第二个点的横坐标： ");
   c=scan.nextDouble();
  
   System.out.println("请您输入第二个点的纵坐标： ");
   d=scan.nextDouble();
   
   e=a-c;
   f=b-d;
   g=Math.pow(e,2);
   h=Math.pow(f,2);
   i=g+h;
   j=Math.sqrt(i);
   System.out.println("("+a+","+b+"). "+"("+c+","+d+")"+"的距离为： "+j);
   }
}
