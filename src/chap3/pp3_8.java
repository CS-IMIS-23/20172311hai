//************************************************************
// pp3_8.java           作者：赵晓海
//
// 产生随机整数，求其正弦，余弦，正切。
//************************************************************
  
  import java.util.Random;
  
  public class pp3_8
  {
  public static void main(String [] args)
  {
  Random generate = new Random();
  
   int a;
   double b,c,d,e;
   
  a=generate.nextInt(21)+20;
  
  System.out.println("产生的随机数为："+a);
 
  b=Math.sin(a);
  c=Math.cos(a);
  d=Math.tan(a);

  System.out.println(a+"的正弦，余弦，正切分别为： "+b+","+c+","+d);
   }
}
