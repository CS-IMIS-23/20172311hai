package Calculator;
/*
Calculate.java         作者：赵晓海  黄宇瑭
用于计算一个后缀表达式
 */
import chap7.RationalNumber;

import java.util.Stack;

public class Calculate {

    String [] str1;
    String result;//计算结果

    //构造函数
    public Calculate (String calculate){
        str1=calculate.split("\\s");
    }

    //将字符串形式的整数或者分数转换为RationalNumber类的对象的方法
    public RationalNumber toRationalNumber(String num){
        //if (num.length()==1){
        if (num.contains("/")==false){
            int a =Integer.parseInt(num);
            RationalNumber rationalNumber1 =new RationalNumber(a,1);
            return rationalNumber1;
        }
        else {
           /* StringTokenizer st = new StringTokenizer(num,"/");
            int numerator =Integer.parseInt(st.nextToken("/"));
            int denominator=Integer.parseInt(st.nextToken("/"));
            */
           String[] Array =num.split("/");
           int numerator = Integer.parseInt(Array[0]);
           int denominator=Integer.parseInt(Array[1]);

            RationalNumber rationalNumber2 =new RationalNumber(numerator,denominator);
            return rationalNumber2;
        }
    }

    //计算中缀表达式，并将结果保存在result中的方法
    public void ToResult(){
        Stack stack1=new Stack();
        int start =0;

        while (start<str1.length){
            if (str1[start].equalsIgnoreCase("+")==false&&str1[start].equalsIgnoreCase("-")==false&&str1[start].equalsIgnoreCase("×")==false&&str1[start].equalsIgnoreCase("÷")==false){

                stack1.push(str1[start]);
            }
            else
                if (str1[start].equalsIgnoreCase("+")==true){
                RationalNumber num1=this.toRationalNumber(String.valueOf(stack1.peek()));
                stack1.pop();
                RationalNumber num2=this.toRationalNumber(String.valueOf(stack1.peek()));
                stack1.pop();
                RationalNumber finish =num2.add(num1);
                String str3=finish.toString();
                stack1.push(str3);
                }
                else
                    if (str1[start].equalsIgnoreCase("-")==true){
                        RationalNumber num1=this.toRationalNumber(stack1.peek().toString());
                        stack1.pop();
                        RationalNumber num2=this.toRationalNumber(stack1.peek().toString());
                        stack1.pop();
                        RationalNumber finish =num2.subtract(num1);
                        String str3=finish.toString();
                        stack1.push(str3);
                    }
                    else
                        if (str1[start].equalsIgnoreCase("×")==true){
                            RationalNumber num1=this.toRationalNumber(stack1.peek().toString());
                            stack1.pop();
                            RationalNumber num2=this.toRationalNumber(stack1.peek().toString());
                            stack1.pop();
                            RationalNumber finish =num2.multiply(num1);
                            String str3=finish.toString();
                            stack1.push(str3);
                        }
                        else
                        {
                            RationalNumber num1=this.toRationalNumber(String.valueOf(stack1.peek()));
                            stack1.pop();
                            RationalNumber num2=this.toRationalNumber(String.valueOf(stack1.peek()));
                            stack1.pop();
                            RationalNumber finish =num2.divide(num1);
                            String str3=finish.toString();
                            stack1.push(str3);
                        }

                start++;
        }
        String str4=stack1.peek().toString();
        result=str4;
    }

    //得到计算结果的方法
    public String getResult() {
        return result;
    }

}
