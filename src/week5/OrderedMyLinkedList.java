package week5;


import week2.LinearNode;

public class OrderedMyLinkedList<T> extends MyLinkedList<T> implements OrderedListADT<T> {
    @Override
    public void add(T element) {
        LinearNode<T> node = new LinearNode(element);
        LinearNode<T> temp1 = null, temp2 = head;
        while (temp2 != null && node.compareTo(temp2.getElement()) > 0) {
            temp1 = temp2;
            temp2 = temp2.getNext();
        }
        if (temp1 == null) {
            head = tail = node;
        } else {
            temp1.setNext(node);
        }
        node.setNext(temp2);
        if (node.getNext() == null)
            tail = node;

        count++;
        modCount++;
    }


}