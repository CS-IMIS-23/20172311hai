package week5;

//列表中元素不可比较时抛出异常
public class NonComparableElementException extends RuntimeException
{

    public NonComparableElementException (String array)
    {
        super ("The " + array + " requires comparable elements.");
    }
}