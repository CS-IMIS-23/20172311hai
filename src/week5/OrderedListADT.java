package week5;

//有序列表特有接口
public interface OrderedListADT<T> extends ListADT<T> {

    public void add(T element);
}