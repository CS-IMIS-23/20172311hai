package week5;

import java.util.Arrays;
import java.util.Iterator;

public class MyArrayList<T> implements Iterable<T>, ListADT<T> {

    private final static int DEFAULT_CAPACITY = 100;
    private final static int NOT_FOUND = -1;

    protected int rear;   // 列表中的元素数目
    protected T[] list;   // 列表
    protected int modCount;
    public MyArrayList() {
        this(DEFAULT_CAPACITY);
    }
    public MyArrayList(int initialCapacity) {
        rear = 0;
        list = (T[]) (new Object[initialCapacity]);
        modCount = 0;
    }
    //查找列表中是否有元素elem，如果有返回索引值，如果没有返回-1
    private int find(T elem) {
        int scan = 0;
        int result = NOT_FOUND;

        if (!isEmpty())
            while (result == NOT_FOUND && scan < rear) {
                if (elem.equals(list[scan]))
                    result = scan;
                else
                    scan++;
            }
        return result;
    }

    //判断是否有元素target
    @Override
    public boolean contains(T target) {
        return (find(target) != NOT_FOUND);
    }


    @Override
    public T removeFirst() {
        T result;
        if (isEmpty())
            throw new ElementNotFoundException("MyArrayList");
        else {
            result = list[0];
            rear--;
            for(int a = 0; a < rear; a++){
                list[a] = list[a + 1];
            }
            list[rear] = null;
        }
        return result;
    }

    @Override
    public T removeLast() {
        T result = list[rear - 1];
        list[rear] = null;
        rear--;
        return result;
    }

    @Override
    public T remove(T elem) {
        T result;
        int index = find(elem);
        if (index == NOT_FOUND) {
            throw new ElementNotFoundException("MyArrayList");
        }
        result = list[index];
        rear--;

        for (int i = index; i < rear; i++) {
            list[i] = list[i + 1];
        }
        list[rear] = null;
        modCount++;
        return result;
    }

    @Override
    public boolean isEmpty() {
        return rear == 0;
    }

    @Override
    public int size() {
        return rear;
    }

    @Override
    public T first() {
        if (isEmpty())
            throw new ElementNotFoundException("MyArrayList");
        else
            return list[0];
    }

    @Override
    public T last() {
        if (isEmpty())
            throw new ElementNotFoundException("MyArrayList");
        else
            return list[rear - 1];
    }

    @Override
    public String toString() {
        String result = "";
        for(int a = 0; a < rear; a++){
            result += list[a] +"\n";
        }
        return result;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    protected void expandCapacity() {
        //复制旧数组并扩容
        list = Arrays.copyOf(list, list.length * 2);
    }


}
