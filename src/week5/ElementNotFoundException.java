package week5;

//删除时没有相应元素抛出异常
public class ElementNotFoundException extends RuntimeException
{

    public ElementNotFoundException (String array)
    {
        super ("The element is not in this " + array);
    }
}
