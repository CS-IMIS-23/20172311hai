package week5;


import week2.EmptyCollectionException;
import week2.LinearNode;

import java.io.*;
import java.util.Iterator;

public class ProgramOfStudy2 implements Iterable<Course>, Serializable {
    private int count;
    private LinearNode<Course> head, tail;

    public ProgramOfStudy2() {
        count = 0;
        head = tail = null;
    }

    public void courseAdd(Course target) {
        LinearNode<Course> node = new LinearNode(target);
        LinearNode<Course> temp1 = null,temp2 = head;

        while(temp2!=null&&node.compareTo(temp2.getElement())>0){
            temp1 = temp2;
            temp2 = temp2.getNext();
        }
        if(temp1 == null){
            head = tail =  node;
        }
        else{
            temp1.setNext(node);
        }

        node.setNext(temp2);

        if(node.getNext() == null)
            tail = node;

        count++;
    }

    public Course removeFirst() {
        if (isEmpty())
            throw new EmptyCollectionException("MyLinkedList");
        LinearNode<Course> current = head;
        if (size() == 1)
            head = tail = null;
        else
            head = current.getNext();

        count--;
        return current.getElement();
    }

    public Course removeLast() {
        if (isEmpty())
            throw new EmptyCollectionException("MyLinkedList");
        boolean found = false;

        LinearNode<Course> previous = null;
        LinearNode<Course> current = head;

        while (current != null && !found) {
            if (current.equals(tail)) {
                found = true;
            }
            else {
                previous = current;
                current = current.getNext();
            }
        }

        if (size() == 1)
            head = tail = null;
        else {
            tail = previous;
            tail.setNext(null);
        }
        count--;
        return current.getElement();
    }

    public Course remove(Course target) {
        if (isEmpty())
            throw new EmptyCollectionException("MyLinkedList");
        boolean found = false;
        LinearNode<Course> previous = null;
        LinearNode<Course> current = head;
        while (current != null && !found)
            if (target.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            throw new ElementNotFoundException("MyLinkedList");

        if (size() == 1)
            head = tail = null;
        else if (current.equals(head))
        head = current.getNext();
        else if (current.equals(tail))
        {
            tail = previous;
            tail.setNext(null);
        } else
            previous.setNext(current.getNext());

        count--;
        return current.getElement();
    }

    public Course first() {
        if (isEmpty())
            throw new EmptyCollectionException("MyLinkedList");
        else
            return head.getElement();
    }

    public Course last() {
        if (isEmpty())
            throw new EmptyCollectionException("MyLinkedList");
        else
            return tail.getElement();
    }

    public boolean contains(Course elem) {
        LinearNode temp = head;
        while(temp !=  null&&!(temp.getElement().equals(elem))){
            temp = temp.getNext();
        }
        if (temp==null)
            return false;
        else
            return true;
    }

    @Override
    public String toString()
    {
        LinearNode<Course> temp = head;
        String result = "";
        while(temp != null)
        {
            result += temp.getElement()+ " "+"\n";
            temp = temp.getNext();
        }
        return result;
    }

    public int size() {
        return count;
    }

    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public Iterator<Course> iterator() {
        return null;
    }

    public void save(String fileName) throws IOException
    {
        FileOutputStream fos = new FileOutputStream(fileName);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this);
        oos.flush();
        oos.close();
    }


    public static ProgramOfStudy2 load(String fileName) throws IOException, ClassNotFoundException
    {
        FileInputStream fis = new FileInputStream(fileName);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ProgramOfStudy2 pos = (ProgramOfStudy2) ois.readObject();
        ois.close();

        return pos;
    }
}
