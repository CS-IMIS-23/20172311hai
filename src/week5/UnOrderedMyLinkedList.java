package week5;

import week2.LinearNode;

public class UnOrderedMyLinkedList<T> extends MyLinkedList<T> implements UnorderedListADT<T> {
    @Override
    public void addToFront(T element) {
        LinearNode<T> node = new LinearNode<T>(element);
        LinearNode<T> temp = head;
        if (isEmpty())
            head = node;
        else {
            node.setNext(head);
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            tail = temp;
            head = node;
        }
        count++;
        modCount++;
    }

    @Override
    public void addToRear(T element) {
        LinearNode<T> node = new LinearNode<T>(element);
        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }

    @Override
    public void addAfter(T element, T target) {
        LinearNode<T> node = new LinearNode<T>(element);
        LinearNode<T> temp = head;
        if (temp == null) {
            head = tail = node;
        }

        while ((temp != null) && (temp.getElement() == target)) {
            temp = temp.getNext();
        }

        if (temp.getNext() == null) {
            temp.setNext(node);
            tail = node;
        } else {
            node.setNext(temp.getNext());
            temp.setNext(node);
        }
        count++;
        modCount++;
    }
}