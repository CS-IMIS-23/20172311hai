package week5;

public class UnorderedMyArrayListTest {
    public static void main(String[] args) {
       UnorderedMyArrayList pos=new UnorderedMyArrayList();
       pos.addToRear(1);
       pos.addToRear(2);
       pos.addToRear("hah");
       pos.addToRear("hello");
       pos.addToRear(3);
       pos.addToRear(5);
       System.out.println("尾插法形成的初始列表："+"\n"+pos);

       System.out.println("前插addtoFront以及在3后面插入addafter3后的链表：");
       pos.addToFront("addtoFront");
       pos.addAfter("addafter3",3 );
       System.out.println(pos);

        System.out.println("列表是否为空："+pos.isEmpty());
        System.out.println("列表长度："+pos.size());
        System.out.println("列表中是否有int型3："+pos.contains(3));

        System.out.println("列表头："+pos.first());
        System.out.println("列表尾:"+pos.last());
        pos.remove(3);
        pos.removeFirst();
        pos.removeLast();
        System.out.println("去掉列表头列表尾和元素3后的列表："+"\n"+pos);
    }
}
