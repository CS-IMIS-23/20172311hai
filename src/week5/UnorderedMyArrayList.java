package week5;

public class UnorderedMyArrayList<T> extends MyArrayList<T> implements UnorderedListADT<T> {
    @Override
    public void addToFront(T elem) {

        if (size() == list.length)
            expandCapacity();
        for (int temp = rear; temp > 0; temp--)
            list[temp] = list[temp - 1];

        list[0] = elem;
        rear++;
        modCount++;
    }
    @Override
    public void addToRear(T elem) {

        if (size() == list.length)
            expandCapacity();

        list[rear] = elem;
        rear++;
        modCount++;

    }
    @Override
    public void addAfter(T element, T target) {
        if (size() == list.length)
            expandCapacity();

        int scan = 0;
        while (scan < rear && !target.equals(list[scan]))
            scan++;

        if (scan == rear)
            throw new ElementNotFoundException("UnorderedList");
        scan++;

        for (int shift = rear; shift > scan; shift--)
            list[shift] = list[shift - 1];

        list[scan] = element;
        rear++;
        modCount++;
    }
}
