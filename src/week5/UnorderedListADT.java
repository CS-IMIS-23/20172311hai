package week5;

//无序列表特有接口
public interface UnorderedListADT<T> extends Iterable<T> {
    public void addToFront(T element);

    public void addToRear(T element);

    public void addAfter(T element, T target);


}
