package week5;

public class OrderedLinkedListTest {
    public static void main(String[] args) {
        OrderedMyLinkedList a=new OrderedMyLinkedList();
        a.add(1);
        a.add(2);
        a.add(5);
        a.add("haha");
        a.add("a");

        System.out.println(a);
        System.out.println("列表中是否有a"+" "+a.contains("a"));

        System.out.println("列表是否为空："+a.isEmpty());
        System.out.println("列表长度："+a.size());

        System.out.println("列表的头："+a.first());
        System.out.println("列表的尾："+a.last());

        a.removeFirst();
        a.removeLast();
        System.out.println("删除列表头和尾后的新列表:"+"\n"+a.toString());

        a.remove(5);
        System.out.println("删除5后的新列表："+"\n"+a.toString());
    }
}
