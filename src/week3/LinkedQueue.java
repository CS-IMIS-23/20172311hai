package week3;

import week2.EmptyCollectionException;
import week2.LinearNode;

/**
 * LinkedQueue represents a linked implementation of a queue.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedQueue<T> implements QueueADT<T>
{
    private int count;
    private LinearNode<T> head, tail;

    /**
     * Creates an empty queue.
     */
    public LinkedQueue()
    {
        count = 0;
        head = tail = null;
    }

    /**
     * Adds the specified element to the tail of this queue.
     * @param element the element to be added to the tail of the queue
     */
    @Override
    public void enqueue(T element)
    {
        LinearNode<T> node = new LinearNode<T>(element);

        if (isEmpty())
            head = node;
        else
            tail.setNext(node);

        tail = node;
        count++;
    }

    /**
     * Removes the element at the head of this queue and returns a
     * reference to it.
     * @return the element at the head of this queue
     * @throws EmptyCollectionException if the queue is empty
     */
    @Override
    public T dequeue() throws EmptyCollectionException
    {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        else {

            T result = head.getElement();
            head = head.getNext();
            count--;

            if (isEmpty())
                tail = null;

            return result;
        }
    }

    /**
     * Returns a reference to the element at the head of this queue.
     * The element is not removed from the queue.

     */
    @Override
    public T first() throws EmptyCollectionException
    {
        if(isEmpty())
            throw new EmptyCollectionException("queue");
        else {
            T result=head.getElement();
            return result;
        }
    }

    /**
     * Returns true if this queue is empty and false otherwise.
     * @return true if this queue is empty
     */
    @Override
    public boolean isEmpty()
    {
        if(count==0)
            return true;
        else
            return false;
    }

    /**
     * Returns the number of elements currently in this queue.
     * @return the number of elements in the queue
     */
    @Override
    public int size()
    {
        return count;
    }

    /**
     * Returns a string representation of this queue.
     * @return the string representation of the queue
     */
    @Override
    public String toString()
    {
        LinearNode temp= head;
        String Final="";
        while (temp!=null){
            Final+=temp.getElement()+" ";
            temp=temp.getNext();
        }
        return Final;

    }
}

