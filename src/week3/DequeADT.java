package week3;

public interface DequeADT <T> {
    //头插法
    public void addFirst(T t);

    //尾插法
    public void addLast(T t);

    //头删法
    public T removeFirst();

    //尾删法
    public T removeLast();

    //返回头
    public T first();


    //判断是否为空
    public boolean isEmpty();

    //返回对列长度
    public int size();

    //打印队列

    public String toString();
}
