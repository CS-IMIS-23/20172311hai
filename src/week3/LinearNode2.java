package week3;

public class LinearNode2<T> {
    protected LinearNode2<T> next;
    protected LinearNode2<T> previous;
    protected T element;

    // 创建空指针
    public LinearNode2()
    {
        previous=null;
        next = null;
        element = null;
    }

    // 创建指针元素
    public LinearNode2 (T elem)
    {
        next = null;
        previous=null;
        element = elem;
    }

    // 返回指针next
    public LinearNode2<T> getNext()
    {
        return next;
    }
    //返回指针previous


    public LinearNode2<T> getPrevious() {
        return previous;
    }

    // 更改指针next
    public void setNext (LinearNode2<T> node)
    {
        next = node;
    }
    //更改指针previous
    public void setPrevious(LinearNode2<T>node2){
        previous=node2;
    }

    // 得到头指针
    public T getElement()

    {
        return element;
    }

    // 更改头指针
    public void setElement (T elem)

    {
        element = elem;
    }
}