package week3;

import week2.EmptyCollectionException;

public class LinkedDeque <T> implements DequeADT<T> {
    private int count;
    private LinearNode2<T> head, tail;

    public LinkedDeque() {
        count = 0;
        head = null;
        tail = null;
    }

    @Override
    public void addFirst(T t) {
        if (isEmpty()) {
            head = new LinearNode2();
            head.element = t;
            tail = head;
        } else {
            LinearNode2 oldHead = head;
            head = new LinearNode2();
            head.setElement(t);
            head.setNext(oldHead);
            oldHead.previous = head;
        }
        count++;
        //        LinearNode2<T>node2=new LinearNode2(t);
//
//        if(isEmpty()){
//            head=node2;
//            tail=node2;
//        }
//        else{
//
//            node2.setNext(head);
//            head.setPrevious(node2);
//            head=node2;
//        }
//        count++;
    }

    @Override
    public void addLast(T t) {
        if (isEmpty()) {
            tail = new LinearNode2();
            tail.element = t;
            head = tail;
        } else {
            LinearNode2 oldTail = tail;
            tail = new LinearNode2();
            tail.element = t;
            tail.previous = oldTail;
            oldTail.next = tail;
        }
        count++;
        //        LinearNode2<T>node2=new LinearNode2(t);
//
//        if (isEmpty()){
//            head=node2;
//            tail=node2;
//        }
//        else {
//            node2.setPrevious(tail);
//            tail.setNext(node2);
//            tail=node2;
//        }
//        count++;
    }

    @Override
    public T removeFirst() throws EmptyCollectionException {

        if (isEmpty()) {
            throw new EmptyCollectionException("deque");
        }
        T result = head.element;
        head = head.next;
        if (count == 1) {
            head = tail = null;//当只有一个节点时也准备出的时候，last和first同时置null
        } else {
            head.previous = null;//丢弃前一个节点
        }
        count--;
        return result;

//        LinearNode2<T> temp = head;
//        if (isEmpty())
//            throw new EmptyCollectionException("deque");
//        else {
//            T result = temp.getElement();
//            head = temp.getNext();
//            count--;
//
//            if (isEmpty())
//            {
//                head = tail = null;
//            }
//            return result;
//        }
    }

    @Override
    public T removeLast() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("deque");
        }
        T result = tail.element;
        tail = tail.previous;
        if (count == 1) {
            head = tail = null;
        } else {
            tail.next = null;
        }
        count--;
        return result;

//        LinearNode2<T>temp=tail;
//        if (isEmpty())
//            throw new EmptyCollectionException("deque");
//        else {
//            T result = temp.getElement();
//            tail = temp.getPrevious();
//            count--;
//
//            if (isEmpty()){
//                head =tail= null;
//                        }
//            return result;
//        }
    }

    @Override
    public T first() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("deque");
        else {
            T result = head.element;
            return result;
        }
    }


    @Override
    public boolean isEmpty() {
        if (count == 0)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        String result = "";
        LinearNode2 current = head;
        while (current != null) {
            result += current.element + " ";
            current = current.next;
        }
        return result;

    }
    //        String result="";
//        if (isEmpty())
//            System.out.println("当前队列为空");
//        else {
//
//            for (int a=0;a<count;a++){
//                result+=head.getElement()+" ";
//                head=head.getNext();
//            }
//        }
//        return result;
//    }
}