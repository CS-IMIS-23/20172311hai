package week3;

public class CircularArrayQueueTest {
    public static void main(String[] args) {

        CircularArrayQueue circularArrayQueue=new CircularArrayQueue();

        System.out.println("往队列里依次添加元素：1 2 4 哈哈 你好");
        circularArrayQueue.enqueue(1);
        circularArrayQueue.enqueue(2);
        circularArrayQueue.enqueue(4);
        circularArrayQueue.enqueue("哈哈");
        circularArrayQueue.enqueue("你好");
        System.out.println("打印队列："+ circularArrayQueue.toString());

        System.out.println("判断是否为空："+circularArrayQueue.isEmpty());

        circularArrayQueue.dequeue();
        circularArrayQueue.dequeue();


        System.out.println("打印删除头两个元素后的队列："+circularArrayQueue.toString());
        System.out.println("当前队列的长度："+circularArrayQueue.size());

        System.out.println("打印当前队列头一个元素："+circularArrayQueue.first());

        System.out.println("使用四次删除元素弹出报错信息：");
        circularArrayQueue.dequeue();
        circularArrayQueue.dequeue();
        circularArrayQueue.dequeue();
        circularArrayQueue.dequeue();

    }
}
