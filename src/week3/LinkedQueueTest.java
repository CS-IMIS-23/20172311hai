package week3;

public class LinkedQueueTest {
    public static void main(String[] args) {
        LinkedQueue linkedQueue=new LinkedQueue();
        System.out.println("往队列里依次添加元素：1 2 4 哈哈 你好");
        linkedQueue.enqueue(1);
        linkedQueue.enqueue(2);
        linkedQueue.enqueue(4);
        linkedQueue.enqueue("哈哈");
        linkedQueue.enqueue("你好");
        System.out.println("打印队列："+ linkedQueue.toString());

        System.out.println("判断是否为空："+linkedQueue.isEmpty());

        linkedQueue.dequeue();
        linkedQueue.dequeue();


        System.out.println("打印删除头两个元素后的队列："+linkedQueue.toString());
        System.out.println("当前队列的长度："+linkedQueue.size());

        System.out.println("打印当前队列头一个元素："+linkedQueue.first());

        System.out.println("使用四次删除元素弹出报错信息：");
        linkedQueue.dequeue();
        linkedQueue.dequeue();
        linkedQueue.dequeue();
        linkedQueue.dequeue();
    }
}
