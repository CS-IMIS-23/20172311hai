package week3;

public class LinkedDequeTest {
    public static void main(String[] args) {
        LinkedDeque linkedDeque=new LinkedDeque();

        System.out.println("空队列的长度："+linkedDeque.size());
        System.out.println("空队列是否为空："+linkedDeque.isEmpty());
//        System.out.println("空队列执行removeFirst()方法的报错信息:"+linkedDeque.removeFirst());
//        System.out.println("空队列执行removeLast()方法的报错信息:"+linkedDeque.removeLast());
//        System.out.println("空队列执行first()方法的报错信息:"+linkedDeque.first());
//        System.out.println("空队列执行toString()方法的提示信息:"+linkedDeque.toString());
        //尾插法
        linkedDeque.addLast(1);
        linkedDeque.addLast(2);
        linkedDeque.addLast("你好");
        linkedDeque.addLast("haha");

        //头插法
        linkedDeque.addFirst(3);
        linkedDeque.addFirst(4);
        linkedDeque.addFirst(5);

        System.out.println("当前队列的长度："+linkedDeque.size());
        System.out.println("当前队列是否为空："+linkedDeque.isEmpty());

        System.out.println("打印当前队列："+linkedDeque.toString());

        System.out.println("分别执行一次头删法和尾删法");

        //头删法
        linkedDeque.removeFirst();

        //尾删法
        linkedDeque.removeLast();
        System.out.println("打印当前队列："+linkedDeque.toString());

        System.out.println("当前队列的长度："+linkedDeque.size());

        System.out.println("执行第六次尾删法时的报错信息：");
        linkedDeque.removeLast();
        linkedDeque.removeLast();
        linkedDeque.removeLast();
        linkedDeque.removeLast();
        linkedDeque.removeLast();
        linkedDeque.removeLast();


    }
}
