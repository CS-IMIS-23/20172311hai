package chap10;

//*************************************************************
//  Firm2.java      Author:zhaoxiaohai
//
//  使用接口Payable实现多态。
//*************************************************************
public class Firm2 {
    public static void main(String[] args)
    {
        Payable personnel = new Staff2();

        personnel.payday();
    }
}
