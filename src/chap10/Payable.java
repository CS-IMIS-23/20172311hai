package chap10;
/*
PP10.1  Payable接口，包含一个payday方法。
 */
public interface Payable {
        public void payday();
    }
