package chap10;


import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

public class IoTest {
    //排序的静态方法

    static void InsertSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int temp = arr[i];
            int j = i - 1;
            while (j >= 0 && temp < arr[j]) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = temp;
        }
    }

    public static void main(String[] args) {

        //输入数个整数
        String num;
        System.out.println("请您输入任意个整数，并且使用空格将不同整数分隔开： ");

        Scanner scan = new Scanner(System.in);

        num = scan.nextLine();

        //将整数写入文件
        FileWriter fw = null;
        try {
            fw = new FileWriter("IOdemo.txt");
            fw.write(num);
        } catch (IOException e) {
            System.out.println("Catch:" + e.toString());
        } finally {

            try {
                if (fw != null)
                    fw.flush();
            } catch (IOException e) {
                System.out.println(e.toString());
            }
        }
        //从文件读取所有的整数
        String str="";
        try {
            Reader reader = new FileReader("IOdemo.txt");

            char[] chars = new char[10];
            int length = reader.read(chars);
            for (int d = 0; d < chars.length; d++) {
                str += chars[d];
            }

            System.out.println("从文件中读取出来的是:" + str);
            reader.close();
        }
        catch (IOException e) {
            System.out.println(e.toString());
        }

        //对读取出来的整数进行排序

        String[] str1=num.split("\\s");
        int[] num4 = new int[str1.length];
        for (int n = 0; n < str1.length ; n++)
        {
            num4[n] = Integer.parseInt(str1[n]);
        }
        InsertSort(num4);
        String str3="";
        for (int a = 0; a < num4.length; a++) {
            str3 = str3 + num4[a] + " ";
        }
        System.out.println("排序之后的结果是： "+str3);

      /* String[] str1 = str.split("\\s");
      //  System.out.println(str1[0]);
        String[]str2 =str1;
        int[] num4 = new int[str2.length];
        for (int n = 0; n < str2.length ; n++)
        {
            num4[n] = Integer.parseInt(str2[n]);
        }
        InsertSort(num4);
        String str3="";
        for (int a = 0; a < num4.length; a++) {
            str3 = str3 + num4[a] + " ";
        }
        System.out.println("排序之后的结果是： "+str3);
*/

        //将排好序的整数写入文件
        try {
            fw.write("  排序后："+str3);
        }
        catch (IOException e)
        {
            System.out.println(e.toString());
        }
        finally {
            try{
            fw.flush();
            }
            catch (IOException e){
                System.out.println(e.toString());
            }

        }

    }

}

