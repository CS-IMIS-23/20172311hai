package chap10;

    //*************************************************************************
//  DVD.java        Author:zhaoxiaohai
//
//  Represents a DVD video disc.
//*********************************************************************

import java.text.NumberFormat;

    public class DVD2 implements Comparable
    {
        private String title, director;
        private int year;
        private double cost;
        private boolean bluray;

        //--------------------------------------------------------------------------
        //  Creates a new DVD with the specified information.
        //-----------------------------------------------------------------------
        public DVD2(String title, String director, int year, double cost,boolean bluray)
        {
            this.title = title;
            this.director = director;
            this.year = year;
            this.cost = cost;
            this.bluray=bluray;

        }

        //---------------------------------------------------------------------------------------------
        //  Returns a string description of this DVD.
        //---------------------------------------------------------------------------------------------
        public String toString()
        {
            NumberFormat fmt = NumberFormat.getCurrencyInstance();
            String description;

            description = title +"\t" +director;
            description += " "+fmt.format(cost) + "\t" +" "+ year +"\t";

            if (bluray)
                description += "\t" + "Blu-ray";

            return description;

        }

        public String getTitle() {
            return title;
        }

        public String getDirector() {
            return director;
        }


        public boolean equals(Object other)
        {
            return (title.equals(((DVD2)other).getTitle())&&director.equals(((DVD2)other).getDirector()));

        }
        public int compareTo(Object other)
        {
            int result;
            String otherTitle=((DVD2)other).getTitle();
            String otherDirector=((DVD2)other).getDirector();
            if(title.equals(otherTitle))
                result=director.compareTo(otherDirector);
            else
                result=title.compareTo(otherTitle);
            return result;
        }
    }
