package week11;

public class ManageNetworkTest
{
    public static void main(String[] args) {
        ManageNetwork<String> manageNetwork = new ManageNetwork<>();
        manageNetwork.addCity("济南");
        manageNetwork.addCity("北京");
        manageNetwork.addCity("天津");
        manageNetwork.addCity("南京");
        manageNetwork.addCity("宝鸡");

        manageNetwork.addEdge(0,1,100);
        manageNetwork.addEdge(0,2,300);
        manageNetwork.addEdge(1,2,50);
        manageNetwork.addEdge(1,3,200);
        manageNetwork.addEdge(2,4,100);
        manageNetwork.addEdge(3,4,50);
        manageNetwork.addEdge(0,4,500);

        System.out.println(manageNetwork.toString());
        manageNetwork.getShortestWeightPath("济南","宝鸡");

        manageNetwork.getShortestPath("济南","宝鸡");
    }
}
