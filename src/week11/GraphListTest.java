package week11;

public class GraphListTest {
    public static void main(String[] args) {
        GraphList<String> graphList = new GraphList<String>();

        graphList.addVertex("4");
        graphList.addVertex("3");
        graphList.addVertex("8");
        graphList.addVertex("10");
        graphList.addVertex("0");

        graphList.addEdge("3","4");
        graphList.addEdge("0","4");
        System.out.println(graphList.toString());

        graphList.removeVertex("0");
        System.out.println(graphList.toString());

    }
}