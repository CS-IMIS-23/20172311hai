package week2;

import java.util.Scanner;
import java.util.Stack;

public class pp3_2 {
    public static void main(String[] args) {

        String text;
        Stack stack=new Stack();

        Scanner scanner=new Scanner(System.in);
        System.out.println("请输入一段英文句子：");
        text=scanner.nextLine();

        String [] text2=text.split("\\s");

        String Answer="";

        for(int num=0;num<text2.length;num++){
            Answer+=toFinal(text2[num])+" ";
        }
        System.out.println(Answer);

    }

    public static String toFinal(String str){
        Stack stack=new Stack();

        for (int num=0;num<str.length();num++){
            stack.push(str.charAt(num));
        }
        String Final="";

        while (stack.empty()==false){
            Final+=String.valueOf(stack.pop());
        }
        return Final;

    }
}
