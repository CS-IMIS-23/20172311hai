package week2;

public class LinkedStackTest {
    public static void main(String[] args) {
        LinkedStack linkedStack=new LinkedStack();
        System.out.println("栈中元素输出后都是从栈顶到栈底显示");

        System.out.println("空栈的元素个数" + linkedStack.size());
        System.out.println("栈是否为空：" + linkedStack.isEmpty());

        linkedStack.push("haha");
        linkedStack.push(1);
        linkedStack.push(2.44);
        System.out.println("栈中所有元素:" + linkedStack.toString());
        System.out.println("显示栈顶元素：" + linkedStack.peek());
        System.out.println("栈的长度：" + linkedStack.size());
        System.out.println("栈是否为空：" + linkedStack.isEmpty());

        System.out.println("pop第一次得到的元素："+linkedStack.pop().toString());
        System.out.println("pop一次后栈中元素：" + linkedStack.toString());
        System.out.println("pop两次后的长度" + linkedStack.size());

        System.out.println("pop第二次得到的元素："+linkedStack.pop().toString());
        System.out.println("pop两次后栈中元素：" + linkedStack.toString());
        System.out.println("pop两次后的长度" + linkedStack.size());

        System.out.println("pop第三次得到的元素："+linkedStack.pop().toString());
        System.out.println("pop三次后的长度" + linkedStack.size());
        System.out.println("pop三次后是否为空：" + linkedStack.isEmpty());

        System.out.println("异常测试:pop第四次");
        System.out.println("异常信息："+linkedStack.pop());
    }
}