package week2;

import java.util.Scanner;
import java.util.Stack;

public class PostfixEvaluator {
    private final static char ADD = '+';
    private final static char SUBTRACT='-';
    private final static char MULTIPLY='*';
    private final static char DIVIDE='/';

    private LinkedStack stack ;
    /**
     *Sets up this evalutor by creating a new stack.
     */
    public PostfixEvaluator(){
        stack=new LinkedStack() ;
    }

    public int evaluate(String expr){
        int op1,op2,result = 0;
        String token;
        Scanner parser = new Scanner(expr);

        while (parser.hasNext() ) {
            token = parser.next();

            if (isOperator(token)) {
                op2 = (int)stack.pop();

                op1 = (int)stack.pop();
                result = evaluateSingleOperator(token.charAt(0), op1, op2);
                stack.push(new Integer(result));

            } else
                stack.push(new Integer(Integer.parseInt(token)));
        }

        return result;
    }
    /**
     * Determins if the specified token is an operator.
     * @param token the token to be evaluated
     * @return true if token is operator
     */
    private boolean isOperator(String token){
        return (token .equals("+") || token .equals("-") || token .equals("*") || token .equals("/") );
    }
    /**
     * Performs integer evaluation on a single expression consisting of
     * the specified operator and operands.
     * @param operation operation to be performed
     * @param op1 the first operand
     * @param op2 the second operand
     * @return value of the expression
     */
    private int evaluateSingleOperator (char operation ,int op1,int op2){
        int result=0;

        switch (operation){
            case ADD:
                result =op1+op2;
                break;
            case SUBTRACT :
                result =op1-op2;
                break;
            case MULTIPLY :
                result =op1*op2;
                break;
            case DIVIDE :
                result =op1/op2;
        }

        return result ;
    }
}

