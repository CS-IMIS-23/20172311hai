package week2;

public class LinearNode<T> {
    protected LinearNode<T> next;
    protected T element;

    // 创建空指针
    public LinearNode()
    {
        next = null;
        element = null;
    }

    // 创建指针元素
    public LinearNode (T elem)
    {
        next = null;
        element = elem;
    }

    // 返回指针next
    public LinearNode<T> getNext()
    {
        return next;
    }

    // 更改指针next
    public void setNext (LinearNode<T> node)
    {
        next = node;
    }

    // 得到头指针
    public T getElement()
    {
        return element;
    }

    // 更改头指针
    public void setElement (T elem)
    {
        element = elem;
    }

    //元素的比较
    public int compareTo(T target) {
        String head=String.valueOf(element);
        String temp=String.valueOf(target);
        return head.compareTo(temp);
    }
}

