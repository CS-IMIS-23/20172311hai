package week2;

import java.util.Arrays;
import java.util.EmptyStackException;

public class ArrayStack<T> {
    private final int DEFAULT_CAPACITY =100;
    private int top;
    private T[] stack;

    public ArrayStack(){
        top=0;
        stack=(T[])(new Object[DEFAULT_CAPACITY]);
    }

    public int size()//返回栈中元素的个数
    {
        return top;
    }

    public void push(T element){
        if (size() == stack.length) {
            expandCapacity();
        }
        stack[top] = element;
        top++;
    }

    private void expandCapacity(){
        stack = Arrays.copyOf(stack,stack.length * 2);
    }

    public boolean isEmpty()//判断栈是否为空
    {
        if(top == 0){
            return true;
        }
        else{
            return false;
        }
    }

    public T pop() throws EmptyCollectionException{
        if(isEmpty())
            throw new EmptyCollectionException("stack");
        top--;
        T result =stack[top];
        stack[top]=null;
        return result;
    }

    public T peek()throws EmptyCollectionException //返回栈顶元素
    {
        if (isEmpty())
            throw new EmptyCollectionException("stack");

        return stack[top-1];
    }

    public String toString()//输出String型栈中数据
    {
        String last="";
        for (int a=0;a<size();a++)
        {
            last+="元素"+a+" "+stack[a]+" ";
        }
        return last;
    }


}
