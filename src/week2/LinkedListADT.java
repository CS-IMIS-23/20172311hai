package week2;

public interface LinkedListADT <T> {

    //尾插法
    public void TailInsert (T t);

    //指定位置插入
    public void NumberInsert(int where,T t);

    //删除某一元素，一次删一个且就近
    public void deleteNumber(T t);

    //打印链表
    public String Print();

    //判断链表长度并返回int型长度
    public int amount();

    //将链表中整数从小到大排序
    public void Sorting();

    //删除指定位置指定元素
    public void whereDelete(int where,T num);

    //显示冒泡排序过程
    public String MaopaoSorting();
}
