package week2;

import java.util.Scanner;

public class MyLinkedListTest {
    public static void main(String args[]){
        MyLinkedList List = new MyLinkedList();
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入若干个整数用空格隔开：");
        String First =scanner.nextLine();
        String [] Transform=First.split("\\s");

        for (int num=0;num<Transform.length;num++){
            List.TailInsert(Integer.parseInt(Transform[num]));
        }
        System.out.println("打印形成的链表：");
        System.out.println(List.Print());

        System.out.println("在1号位置添加3后的链表：");
        List.NumberInsert(1,3);
        System.out.println(List.Print());

        System.out.println("在4号位置添加5后的链表：");
        List.NumberInsert(4,5);
        System.out.println(List.Print() );

        System.out.println("在2号位置添加500后的链表：");
        List.NumberInsert(2,200);
        System.out.println(List.Print() );

        System.out.println("删除链表中第一个3后的链表：");
        List.deleteNumber(3);
        System.out.println(List.Print());
        System.out.println("删除链表中第一个5的元素后的链表：");
        List.deleteNumber(5);
        System.out.println(List.Print());
        System.out.println("删除链表中第一个500的元素后的链表：");
        List.deleteNumber(200);
        System.out.println(List.Print()) ;

        System.out.println("链表长度："+List.amount());

        System.out.println("对链表元素从小到大进行排序");
        List.Sorting();
        System.out.println("排序后的链表： " + List.Print());;
    }
}