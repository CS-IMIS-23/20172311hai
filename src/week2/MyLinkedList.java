package week2;

public class MyLinkedList<T>implements LinkedListADT<T> {

    protected LinearNode<T> head;
    protected int nZhaoXiaoHai;

    public MyLinkedList(){
        head=null;
        nZhaoXiaoHai=0;
    }
    @Override
    //尾插法
    public void TailInsert(T t) {

        LinearNode<T> node = new LinearNode(t);
        LinearNode<T> temp;

        if (head == null) {
            head = node;
        }
        else {
            temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
        nZhaoXiaoHai++;
    }

    @Override
    //指定位置插入
    public void NumberInsert(int where, T t) {
        LinearNode<T> node = new LinearNode(t);
        LinearNode<T> temp1, temp2;

        if(nZhaoXiaoHai + 1 >= where){

            if(where == 1)
            {
                node.next = head;
                head = node;

            }
            else
            {
                temp1 = head;
                temp2 = head.next;
                for(int a = 1; a < where - 1; a++)
                {
                    temp1 = temp1.next;
                    temp2 = temp2.next;
                }
                if(temp2 != null)
                {
                    node.next = temp2;
                    temp1.next = node;
                }
                else if(temp2 == null)
                {
                    temp1.next = node;
                }

            }
            nZhaoXiaoHai++;
        }
    }

    @Override
    //删除某一元素，一次删一个且就近
    public void deleteNumber(T t) {

            LinearNode temp = head;

            if(temp.getElement().equals(t))
            {
                head = temp.next;
            }
            else {
                while (!(temp.next.getElement() == t))
                {
                    temp = temp.next;
                }
                temp.next = temp.next.next;
            }
            nZhaoXiaoHai--;

    }

    @Override
    //打印链表
    public String Print() {

        String result = "";
        LinearNode <T>temp = head;
        while(temp != null){
            result += temp.getElement()+ " ";
            temp = temp.next;
        }
        return result;

    }

    @Override
    //判断链表长度并返回int型长度
    public int amount() {
        return nZhaoXiaoHai;
    }

    @Override
    //选择排序
    public void Sorting() {
        LinearNode<T> min;
        T temp;
        LinearNode<T> numNode = head;
        while (numNode!=null)
        {
            min = numNode;
            LinearNode<T> current = min.next;
            while (current!=null)
            {
                if((int)current.getElement()<(int)min.getElement()) {
                    min = current ;
                }
                current = current.next;
            }
            temp= min.getElement();
            min.setElement(numNode.getElement());
            numNode.setElement(temp);
            numNode  = numNode.next;
        }

    }

    @Override
    //删除指定位置指定元素
    public void whereDelete(int where, T num) {
        LinearNode node = new LinearNode(num);
        LinearNode temp1, temp2;
        //头删除
        if((where == 1)&&(head.element.equals(num))){
            head = head.next;
        }
        else{
            if(where <= nZhaoXiaoHai + 1){
                temp1 = head;
                temp2 = head.next;
                for(int a = 1; a < where - 1; a++)
                {
                    temp1 = temp1.next;
                    temp2 = temp2.next;
                }
                //中间删除
                if(temp2.element.equals(node.element)){
                    //中间删除
                    if(temp2.next != null){
                        temp1.next = temp2.next;
                    }

                    //尾删除
                    else{
                        temp1.next = null;
                    }
                }
                else{
                    System.out.println("在第" + where +"位删除节点有问题！");
                }
            }else{
                System.out.println("在第" + where +"位删除节点有问题！");
            }
        }
        nZhaoXiaoHai--;
    }

    @Override
    //显示冒泡排序过程
    public String MaopaoSorting() {
        String [] Linklist=this.Print().split("\\s");
        int [] put=new int[Linklist.length];
        for (int a=0;a<Linklist.length;a++){
            put[a]= Integer.parseInt(Linklist[a]);
        }
        String result="";
        for (int i=0;i<put.length-1;i++){
            //外层循环控制排序趟数
            for (int j=0;j<put.length-1-i;j++){
                //内层循环控制每一趟排序多少次
                if (put[j]>put[j+1]){
                    int temp=put[j];
                    put[j]=put[j+1];
                    put[j+1]=temp;
                }
                String every="";
                for (int a=0;a<put.length;a++){
                    every+=put[a]+" ";
                }
                result+="元素的总数:"+put.length+"当前链表所有元素："+every+"\n";
            }
        }
        return result;
    }


}

//    private Element head;
//    public MyLinkedList(){
//        head = null;
//    }
//
//    //尾插法
//    public void TailInsert(int num)
//    {
//        Element node = new Element(num);
//        Element temp;
//
//        if (head == null) {
//            head = node;
//        } else
//        {
//            temp = head;
//            while (temp.next != null) {
//                temp = temp.next;
//            }
//            temp.next = node;
//        }
//    }
//
//    //指定位置插入
//    public void insertNumber(int where, int num){
//        Element node = new Element(num);
//        Element temp1, temp2;
//
//        if(amount(head) + 1 >= where){
//
//            if(where == 1)
//            {
//                node.next = head;
//                head = node;
//            }
//            else
//            {
//                temp1 = head;
//                temp2 = head.next;
//                for(int a = 1; a < where - 1; a++)
//                {
//                    temp1 = temp1.next;
//                    temp2 = temp2.next;
//                }
//                if(temp2 != null)
//                {
//                    node.next = temp2;
//                    temp1.next = node;
//                }
//                else if(temp2 == null)
//                {
//                    temp1.next = node;
//                }
//            }
//        }
//    }
//
//    //删除某一元素，一次删一个且就近
//    public void deleteNumber(int num)
//    {
//        Element temp = head;
//
//        if(temp.number == num)
//        {
//            head = temp.next;
//        }
//        else {
//            while (!(temp.next.number == num))
//            {
//                temp = temp.next;
//            }
//            temp.next = temp.next.next;
//        }
//    }
//
//    //打印链表
//    public String Print() {
//        String result = "";
//        Element temp = head;
//        while(temp != null){
//            result += temp.number + " ";
//            temp = temp.next;
//        }
//        return result;
//    }
//
//    //判断链表长度并返回int型长度
//    private int amount(Element list){
//        int num = 1;
//        while(list.next != null){
//            list = list.next;
//            num++;
//        }
//        return num;
//    }
//
//    //将链表中整数从小到大排序
//    public void Sorting(){
//        Element min;
//        int temp;
//        Element numNode = head;
//        while (numNode!=null)
//        {
//            min = numNode;
//            Element current = min.next;
//            while (current!=null)
//            {
//                if(current.number<min.number) {
//                    min = current ;
//                }
//                current = current.next;
//            }
//            temp= min.number;
//            min.number = numNode.number;
//            numNode.number = temp;
//
//            numNode  = numNode.next;
//        }
//    }
//    //链表元素类
//
//}