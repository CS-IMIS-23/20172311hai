package week2;


    public class LinkedStack<T>  {

        private int count; // 栈的长度

        private LinearNode<T> top; // 指向栈顶的指针

        //构造函数
        public LinkedStack()
        {
            count = 0;
            top = null;
        }

        //进栈操作方法
        public void push(T element) {
            LinearNode<T> temp = new LinearNode<T>(element);

            temp.setNext(top);
            top = temp;
            count++;
        }

        //出栈操作方法
        public T pop() throws EmptyCollectionException {
            if (isEmpty())
            {
                throw new EmptyCollectionException("stack");
            }

            T result = top.getElement();
            top = top.getNext();
            count --;

            return result;
        }

        //显示栈顶元素方法
        public T peek() {
            if (isEmpty())
            {
                throw new EmptyCollectionException("Stack");
            }
            else
            {
                return top.getElement();
            }
        }

       //判断栈是否为空的方法
        public boolean isEmpty() {
            if (count == 0){
                return true;
            }
            else{
                return false;
            }
        }

        //获得栈中元素个数的方法
        public int size() {
            return count;
        }

        public String toString()//输出String型栈中数据,从栈顶到栈底输出
        {
            String last="";
            LinearNode temp=top;
            while (temp.getNext()!=null){
                last+=temp.getElement().toString()+" ";
                temp=temp.getNext();
            }
            last+=temp.getElement().toString();
            return last;
        }
    }

