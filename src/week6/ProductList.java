package week6;

import week2.EmptyCollectionException;
import week5.ElementNotFoundException;

public class ProductList {

    protected int count;
    protected TLinearNode<Product> head, tail;

    //构造函数
    public ProductList() {
        count = 0;
        head = tail = null;
    }

    //删除第一个Product
    public Product removeFirst() {
        if (isEmpty())
            throw new EmptyCollectionException("ProductList");
        TLinearNode<Product> current = head;
        if (size() == 1)
            head = tail = null;
        else
            head = current.getNext();
        count--;
        return current.getElement();
    }

    //删除最后一个Product
    public Product removeLast() {
        if (isEmpty())
            throw new EmptyCollectionException("ProductList");
        boolean found = false;

        TLinearNode<Product> previous = null;
        TLinearNode<Product> current = head;

        while (current != null && !found) {
            if (current.equals(tail)) {
                found = true;
            }
            else {
                previous = current;
                current = current.getNext();
            }
        }
        if (size() == 1)
            head = tail = null;
        else {
            tail = previous;
            tail.setNext(null);
        }
        count--;
        return current.getElement();
    }

    //删除指定商品
    public Product remove(Product target) {
        if (isEmpty())
            throw new EmptyCollectionException("ProductList");
        boolean found = false;
        TLinearNode<Product> previous = null;
        TLinearNode<Product> current = head;
        while (current != null && !found)
            if (target.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }
        if (!found)
            throw new ElementNotFoundException("ProductList");

        if (size() == 1)
            head = tail = null;
        else if (current.equals(head))
            head = current.getNext();
        else if (current.equals(tail))
        {
            tail = previous;
            tail.setNext(null);
        } else
            previous.setNext(current.getNext());

        count--;
        return current.getElement();
    }

    //返回第一个Product
    public Product first() {
        if (isEmpty())
            throw new EmptyCollectionException("ProductList");
        else
            return head.getElement();
    }

    //返回最后一个Product
    public Product last() {
        if (isEmpty())
            throw new EmptyCollectionException("ProductList");
        else
            return tail.getElement();
    }

    //判断是否包含某一特定商品
    public boolean contains(Product target) {
        TLinearNode temp = head;
        while(temp !=  null&&!(temp.getElement().equals(target))){
            temp = temp.getNext();
        }
        if (temp==null)
            return false;
        else
            return true;
    }

    //返回列表
    @Override
    public String toString()
    {
        TLinearNode<Product> temp = head;
        String result = "";
        while(temp != null)
        {
            result += temp.getElement()+ " "+"\n";
            temp = temp.getNext();
        }
        return result;
    }

    //返回列表中Product的个数
    public int size() {
        return count;
    }

    //判断列表是否为空
    public boolean isEmpty() {
        return count == 0;
    }

    //在列表前面添加Product
    public void addToFront(Product element) {
        TLinearNode<Product> node = new TLinearNode<Product>(element);
        TLinearNode<Product> temp = head;
        if (isEmpty())
            head = node;
        else {
            node.setNext(head);
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            tail = temp;
            head = node;
        }
        count++;
    }

    //在列表尾部添加Product
    public void addToRear(Product element) {
        TLinearNode<Product> node = new TLinearNode<Product>(element);
        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }

    //在列表特定元素后面添加Product
    public void addAfter(Product target, Product newProduct) {
        TLinearNode<Product> node = new TLinearNode<Product>(newProduct);
        TLinearNode<Product> temp = head;
        if (temp == null) {
            head = tail = node;
        }
        while ((temp != null) && !(temp.getElement() .equals(target))) {
            temp = temp.getNext();
        }
        if (temp.getNext() == null) {
            temp.setNext(node);
            tail = node;
        } else {
            node.setNext(temp.getNext());
            temp.setNext(node);
        }
        count++;
    }

    //选择排序
    public void SelectSorting() {
        TLinearNode<Product> min;
        Product temp;
        TLinearNode<Product> numNode = head;
        while (numNode != null) {
            min = numNode;
            TLinearNode<Product> current = min.next;
            while (current != null) {
                if (current.getElement().compareTo(min.getElement()) < 0) {
                    min = current;
                }
                current = current.next;
            }
            temp = min.getElement();
            min.setElement(numNode.getElement());
            numNode.setElement(temp);
            numNode = numNode.next;
        }
    }

    //输入商品名进行查找功能
    public Product find(String target){
        TLinearNode<Product> temp = head;
        while(temp !=  null&&!temp.getElement().getName().equals(target)){
            temp = temp.getNext();
        }
        return temp.getElement();
    }

}
