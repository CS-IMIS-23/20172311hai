package week6;

public class TLinearNode<T> {
    protected TLinearNode<T> next;
    protected T element;

    // 创建空指针
    public TLinearNode()
    {
        next = null;
        element = null;
    }

    // 创建指针元素
    public TLinearNode (T elem)
    {
        next = null;
        element = elem;
    }

    // 返回指针next
    public TLinearNode<T> getNext()
    {
        return next;
    }

    // 更改指针next
    public void setNext (TLinearNode<T> node)
    {
        next = node;
    }

    // 得到头指针
    public T getElement()
    {
        return element;
    }

    // 更改头指针
    public void setElement (T elem)
    {
        element = elem;
    }

    //元素的比较
    public int compareTo(T target) {
        String head=String.valueOf(element);
        String temp=String.valueOf(target);
        return head.compareTo(temp);
    }
}
