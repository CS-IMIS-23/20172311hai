package week6;

public class ProductListTest {
    public static void main(String[] args) {

        ProductList productList=new ProductList();
        System.out.println("初始列表是否为空："+productList.isEmpty());
        System.out.println("初始列表长度："+productList.size());

        productList.addToRear(new Product("h",2,3));
        productList.addToRear(new Product("r",3,7));
        productList.addToRear(new Product("a",10,3));
        productList.addToRear(new Product("h",9,5));
        productList.addToRear(new Product("s",10,4));
        productList.addToRear(new Product("m",1,40));
        productList.addToRear(new Product("z",3,2));

        productList.addToFront(new Product("b",9,2));
        System.out.println("当前列表："+"\n"+productList.toString());

        System.out.println("新列表是否为空："+productList.isEmpty());
        System.out.println("新列表长度"+productList.size());

        System.out.println("返回商品名为a的商品的信息："+productList.find("a"));

        Product a=productList.find("a");
        productList.addAfter(a,new Product("c",2,4));
        System.out.println("在a商品后面加名为c，价格为2，数量为4的新商品后的新列表："+"\n"+productList.toString());

        System.out.println("商品中是否包含名为z，价格为3，数量为2的商品"+productList.contains(new Product("z",3,2)));
        System.out.println("列表头为："+productList.first());
        System.out.println("列表尾为："+productList.removeLast());

        productList.removeLast();
        productList.removeFirst();
        System.out.println("去掉头和尾后的新列表："+"\n"+productList.toString());

        productList.remove(new Product("a",10,3));
        System.out.println("删除商品a后的列表："+"\n"+productList.toString());

        productList.SelectSorting();
        System.out.println("根据先商品名称后价格顺序进行排序后的列表"+"\n"+productList.toString());

    }
}
