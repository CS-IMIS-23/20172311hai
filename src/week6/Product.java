package week6;

public class Product implements Comparable<Product>{

    protected String name;
    protected int price;
    protected int num;

    public Product(String name, int price, int num) {
        this.name = name;
        this.price = price;
        this.num = num;
    }
    public void setName(String name){
        this.name=name;
    }

    public void setPrice(int price){
        this.price=price;
    }

    public void setNum(int num){
        this.num=num;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getNum() {
        return num;
    }

    @Override
    public boolean equals(Object other)
    {
        boolean result = false;
        if (other instanceof Product)
        {
            Product otherProduct = (Product) other;
            if (name.equals(otherProduct.getName()))
                result = true;
        }
        return result;
    }
    @Override
    public int compareTo(Product product) {
        int result;
        String ProductName = product.getName();
        String ProductPrice = String.valueOf(product.price);
        if(name != ProductName){
            result = this.name.compareTo(ProductName);
        }
        else {
            result = String.valueOf(this.price).compareTo(ProductPrice);
        }
        return result;
    }

    @Override
    public String toString()
    {
        String result = "商品名称："+ name + "\t 商品价格：" + price + "\t 商品数量 " + num;
        return result;
    }
}

