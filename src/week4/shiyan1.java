package week4;

import week2.MyLinkedList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class shiyan1 {
    public static void main(String[] args) {

        String input;
        MyLinkedList myLinkedList = new MyLinkedList();

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一些整数，两个整数之间用一个空格符隔开：");
        input = scanner.nextLine();

        String[] trans = input.split("\\s");

        for (int a = 0; a < trans.length; a++) {
            myLinkedList.TailInsert(trans[a]);
        }
        System.out.println("打印所有链表元素：" + myLinkedList.Print());

        System.out.println("当前链表元素总数：" + myLinkedList.amount());

        String fromFile = readString("C:\\Users\\user\\IdeaProjects\\socialsea\\socialsea\\src\\week4\\shiyan2File");

        String[] trans2 = fromFile.split("\\s");

        myLinkedList.NumberInsert(6, trans2[0]);
        System.out.println("链表第5位插入文件中读入的1后的新链表：" + myLinkedList.Print());
        System.out.println("当前链表元素总数:" + myLinkedList.amount());

        myLinkedList.NumberInsert(1, trans2[1]);
        System.out.println("链表第0位插入文件中读入的2后的新链表:" + myLinkedList.Print());
        System.out.println("当前链表元素总数:" + myLinkedList.amount());

        myLinkedList.whereDelete(7, "1");
        System.out.println("从链表中删除刚才的数字1后的链表：" + myLinkedList.Print());
        System.out.println("当前链表元素总数：" + myLinkedList.amount());

//        String [] Linklist=myLinkedList.Print().split("\\s");
//        int [] put=new int[Linklist.length];
//        for (int a=0;a<Linklist.length;a++){
//            put[a]= Integer.parseInt(Linklist[a]);
//        }
        System.out.println("冒泡排序的过程：");
        System.out.println(myLinkedList.MaopaoSorting());


    }

    //将指定文件中的文本保存到指定字符串的方法
    protected static String readString(String filename) {
        String string = "";
        File file = new File(filename);
        try {
            FileInputStream in = new FileInputStream(file);

            //size为字符串的长度，这里一次性读完
            int size = in.available();
            byte[] buffer = new byte[size];
            in.read(buffer);
            in.close();
            string = new String(buffer, "GB2312");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return string;
    }
}

//    //冒泡排序 显示每一步操作后结果
//    protected static String MaoPaoSorting(int[] input){
//        String result="";
//        for (int i=0;i<input.length-1;i++){
//            //外层循环控制排序趟数
//            for (int j=0;j<input.length-1-i;j++){
//                //内层循环控制每一趟排序多少次
//                if (input[j]>input[j+1]){
//                    int temp=input[j];
//                    input[j]=input[j+1];
//                    input[j+1]=temp;
//                }
//                String every="";
//                for (int a=0;a<input.length;a++){
//                    every+=input[a]+" ";
//                }
//                result+="元素的总数:"+input.length+"当前链表所有元素："+every+"\n";
//            }
//        }
//        return result;
//    }
//
//}
