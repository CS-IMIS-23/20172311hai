package week4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class shiyan2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("请您输入若干整数，每个整数之间用一个空格符隔开：");
        String input = scanner.nextLine();

        MyArray myArray = new MyArray(input);

        System.out.println("当前数组元素总数:" + myArray.size());
        System.out.println("当前数组元素：" + myArray.toString());

        String fromFile = readString("C:\\Users\\user\\IdeaProjects\\socialsea\\socialsea\\src\\week4\\shiyan2File");

        String[] trans2 = fromFile.split("\\s");

        myArray.whereInsert(5, trans2[0]);
        System.out.println("数组第5位插入文件中读入的1后的新数组：" + myArray.toString());
        System.out.println("当前数组元素总数:" + myArray.size());

        myArray.whereInsert(0, trans2[1]);
        System.out.println("数组第0位插入文件中读入的2后的新数组:" + myArray.toString());
        System.out.println("当前数组元素总数:" + myArray.size());

        myArray.whereDelete(6);
        System.out.println("从数组中删除刚才的数字1后的数组：" + myArray.toString());
        System.out.println("当前数组元素总数：" + myArray.size());

        System.out.println("显示选择排序的过程：");
        System.out.println(myArray.SelectSorting());
    }


    //将指定文件中的文本保存到指定字符串的方法
    protected static String readString(String filename) {
        String string = "";
        File file = new File(filename);
        try {
            FileInputStream in = new FileInputStream(file);

            //size为字符串的长度，这里一次性读完
            int size = in.available();
            byte[] buffer = new byte[size];
            in.read(buffer);
            in.close();
            string = new String(buffer, "GB2312");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return string;
    }

}
