package week4;
import week3.CircularArrayQueue;
import java.util.Scanner;

public class YangHuiSanJiao {
    public static void main(String[] args) {

        System.out.println("请您输入要生成的杨辉三角的行数：");
        Scanner scanner=new Scanner(System.in);
        int inputN;
        inputN=scanner.nextInt();

        CircularArrayQueue circularArrayQueue=new CircularArrayQueue();

        int a=1;

        while (a<inputN+1){
            if (a==1){
                circularArrayQueue.enqueue(1);
                System.out.println(circularArrayQueue.toString());
            }
            else {
                for (int j=1;j<a+1;j++){
                    if (j==1)
                        circularArrayQueue.enqueue(1);
                    else {
                        if (j>1&&j<a){
                            int first= (int) circularArrayQueue.dequeue();

                            circularArrayQueue.enqueue(first+(int)circularArrayQueue.first());
                        }
                        else {
                            circularArrayQueue.enqueue(1);
                            circularArrayQueue.dequeue();
                        }

                    }
                }
                System.out.println(circularArrayQueue.toString());

            }
            a++;

        }



    }
}
