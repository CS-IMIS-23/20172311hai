package chap8;

/*
AccountCollection.java                 Author:zhaoxiaohai
基于chap4中的Account类，创建并管理多个银行账户
 */
import chap4.Account;
import java.text.NumberFormat;

public class AccountCollection {
     Account[]collection;
    private int count;

    public Account[] getCollection() {
        Account[]copy=new Account[count];
        for(int c=0;c<count;c++)
            copy[c]=collection[c];
        return copy;
    }

    public AccountCollection()
    {
        collection=new Account[30];
        count=0;
    }
    public void addAccount(String name,long acctNumber,double balance)
    {
        collection[count]=new Account(name, acctNumber,balance);
        count++;
    }
    public void allAddRate()
    {
        for(int one=0;one<count;one++)
            collection[one].addRate();

    }
    public double withDraw(int 索引,double amount,double fee)

    {
       return collection[索引].withdraw(amount,fee);
    }
    public double Deposit(int 索引,double amount)
    {
        return collection[索引].deposit(amount);
    }

    public String toString() {
        NumberFormat fmt=NumberFormat.getCurrencyInstance();
        String report="账户信息： \n";
        for(int acc=0;acc<count;acc++)
            report+=collection[acc].toString()+"\n";
        return report;
    }
}
