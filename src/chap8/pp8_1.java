package chap8;
/*
pp8_1.java                 Author:zhaoxiaohai
用于读取0~50（包含二者）范围内的任意多个整数，并且计算每一项输入数据输入的次数。
用一个该范围之外的值表明输入结束。输入完成后，输出所有的值以及出现的次数。
 */
import java.util.Scanner;
public class pp8_1 {
    public static void main(String[] args) {
        final int a=51;
        int input;
        Scanner scan=new Scanner(System.in);
        int[]range=new int[a];

        System.out.print("请您输入一个大于等于0，且小于等于50的整数： ");
        input=scan.nextInt();
        while(input>=0&&input<=50)
        {
            range[input]++;
            System.out.print("请您输入一个大于等于0，且小于等于50的整数继续（或者输入范围之外的整数停止统计）： ");
            input=scan.nextInt();
        }
        System.out.println();
        for(int output=0;output<a;output++)
        {
            System.out.println(output+": "+range[output]);
        }
    }
}
