package chap8;

public class Student {

    public static void main(String ... args) {
        System.out.print(args[0]+"' average score is : ");
        double average;
        int sum=0;
        int num=0;
        for(int a=1 ;a<args.length;a++) {
            sum += Integer.parseInt(args[a]);
            num++;
        }
        average=(double)sum/num;
        System.out.println(average);
    }
}