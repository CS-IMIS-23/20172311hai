package chap8;
/*
LetterCount.java              Author:zhaoxiaohai
Demonstrates the relationship between arrays an string.
 */
import java.util.Scanner;

public class LetterCount {
    /*
    Reads a sentence from the user and counts the number of
    uppercase and lowercase letters contained in it.
     */
    public static void main(String[] args) {
        final int NUMCHARS = 26;
        Scanner scan = new Scanner(System.in);

        int[] upper = new int[NUMCHARS];
        int[] lower = new int[NUMCHARS];

        char current;//the current character being processed.
        int other = 0;//count for nom-alphabetics.

        System.out.println("Enter a sentence: ");
        String line = scan.nextLine();

        //count the number of each letter occurrence
        for (int ch = 0; ch < line.length(); ch++) {
            current = line.charAt(ch);
            if (current >= 'A' && current <= 'Z')
                upper[current - 'A']++;
            else if (current >= 'a' && current <= 'z')
                lower[current - 'a']++;
            else
                other++;
        }
        //prints the results
        System.out.println();
        for (int letter = 0; letter <upper.length; letter++) {
            System.out.print((char) (letter + 'A'));
            System.out.print(": " + upper[letter]);
            System.out.print("\t\t" + (char) (letter + 'a'));
            System.out.println(": " + lower[letter]);
        }
        System.out.println();
        System.out.println("Non-alphabetic characters: " + other);
    }
}
