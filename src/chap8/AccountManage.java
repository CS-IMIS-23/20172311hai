package chap8;
/*
AccountManage.java                Author:zhaoxiaohai
对之前创建的AccountCollection类进行实例化。
 */
import chap4.Account;
public class AccountManage {
    public static void main(String[] args) {
        AccountCollection manage = new AccountCollection();
        manage.addAccount("A", 900711, 30.3);
        manage.addAccount("B", 900712, 500.4);
        manage.addAccount("C", 900713, 40.5);
        Account[] arr = manage.getCollection();
//
//        arr[0].withdraw(30.4, 20.0);
//        System.out.println(arr[0].getBalance());
//
//        arr[0].deposit(20);
//        System.out.println(manage);
//
//        arr[1].withdraw(30, 1);
//        System.out.println(manage);

        for (int a = 0; a < arr.length; a++)
            System.out.println("账户" + a + "的本金加利息是： " + arr[a].addInterest());

        System.out.println();

        System.out.println("使用一次allAddRate使所有账户的利息增加3%后的本金加利息为： ");

        System.out.println();

        manage.allAddRate();
        for (int a = 0; a < arr.length; a++)
            System.out.println("账户" + a + "的本金加利息是： " + arr[a].addInterest());

        manage.withDraw(0, 3.2, 4);
        System.out.println(manage);

        manage.Deposit(1,4.3);
        System.out.println(manage);

        manage.withDraw(0,80,2);
        System.out.println(manage);

    }
}