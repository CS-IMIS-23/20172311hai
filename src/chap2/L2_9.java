//**************************************************************************************************************
//  GasMileage.java         Author:赵晓海
//   
//  Demonstrate the use of the Scanner class to read numeric data.
//**************************************************************************************************************
 
   import java.util.Scanner;
   
   class L2_9
  {
    //------------------------------------------------------------------------------------------------
    //Caculates fuel efficiency based on values entered by the
    //user
    //-----------------------------------------------------------------------------------------------
    public static void main (String [] args)
    {
     int miles;
    double gallons, mpg;
    
    Scanner scan =new Scanner (System.in);
   
    System.out.println("Enter the number of miles: ");
    miles =scan.nextInt();
    
    System.out.println("Enter the gallons of fuel used: ");
    gallons =scan.nextDouble();
    
     mpg=miles/gallons;
     
     System.out.println("Miles Per Gallon: "+ mpg);

     }
}
