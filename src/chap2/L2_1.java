// ppL2_1.java     Auther zhaoxiaohai
//
// Demonstrates the difference between print and println.

class L2_1{
  public static void main(String[]args)
 {
  System.out.print("Three...");
  System.out.print("Two...");
  System.out.print("One...");
  System.out.print("Zero...");
  System.out.println("Liftoff!");//appears on first output line
  System.out.println("Houston,we hava a problem");
 }  

}
