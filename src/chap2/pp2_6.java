//**************************************************************
//The length of the conversion.java        作者：赵晓海
//
//用于将英里数转换为千米数。
//**************************************************************

  import java.util.Scanner;

  public class pp2_6
  {
  public static void main(String [] args)
  {
    double a,b;
    final double C=1.60935;
  
    Scanner scan =new Scanner(System.in);
   
    System.out.println("您输入的英里数将被转换为千米数，请您输入： ");
    a=scan.nextDouble();      //a代表输入的英里数
     
    b=a*C;    //b代表输出的千米数，C代表转换系数。
    
    System.out.println(a+"英里="+b+"千米");
   
    }
} 























