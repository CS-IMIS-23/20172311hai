//***********************************************************
//pp2_11.java            作者：赵晓海
//
//确定每种纸币和硬币需要的最少数量以达到输入的总钱数。
//***********************************************************

  import java.util.Scanner;
  
   public class pp2_11
   {
   public static void main(String [] args)
   {
    double a,b;//a代表输入的钱数,b代表a*100
    
    int c,d,e,f,g,h,i,j,k,l,m,n;
    
    
    Scanner scan=new Scanner (System.in);
    
    System.out.println("请您输入总钱数： ");
    a=scan.nextDouble();
    
    b=a*100.0;
    c=(int)b/1000; //10美元的个数 
    d=(int)b%1000;
    e=d/500;//5美元的个数
    f=d%500;
    g=f/100;//1美元的个数
    h=f%100;
    i=h/25; //25美分的个数
    j=h%25;
    k=j/10; //10美分的个数
    l=j%10;
    m=l/5;//5美分的个数
    n=l%5;//1美分的个数
    
    System.out.println(c+" ten dollar bills\n"+e+" five dollar bills\n"+g+" one dollar bills\n"+i+" quarters\n"+k+" dimes\n"+m+" nickles\n"+n+" pennies");

    }
}


















