//****************************************************************
//  PP2_3.java             作者：赵晓海
//
// 该程序用来读取两个浮点数，然后输出他们的平均值
//
//****************************************************************
   import java.util.Scanner;

   class pp2_3
   {
   public static void main (String [] args)
  {
    double a,b,c;
    
    Scanner scan=new Scanner (System.in);
 
    System.out.println("你将输入两个数并求其平均值，现在请您输入第一个值： ");
    a =scan.nextDouble();
   
    System.out.println("请您继续输入第二个值： ");
    b =scan.nextDouble();

    c=(a+b)/2;
    
    System.out.println("您输入的数据的平均值为: " + c );
   
   }
}
