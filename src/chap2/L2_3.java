//***************************************************************************************
// Addition.java           Auther:zhaoxiaohai
//
//Demonstrate the difference between the addition and string concatenation operators
//***************************************************************************************
 
class L2_3
{
   //-----------------------------------------------------------------------------------
   //Concatenates and adds two numbers and prints the results.
   //-----------------------------------------------------------------------------------
   public static void main (String [] args)
   {
    System.out.println("24 and 45 concatenated: " +24 + 25);
    
   System.out.println("24 and 45 added: " +(24+45));
   }
}

