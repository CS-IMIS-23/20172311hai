//***************************************************************
// Money statistics.java       作者：赵晓海
//
// 用于统计四种硬币之和
//***************************************************************

  import java.util.Scanner;

  public class pp2_10
  {
  public static void main(String [] args)
  {
   int a,b,c,d;//a.b.c.d分别代表25.10.5.1美分的个数.
   
   int e,f;   //e代表输出的美元数，f代表输出的美分数   

   final int A=100;
   
   Scanner scan=new Scanner(System.in);
  
   System.out.println("请您输入对应面值硬币的个数，请先输入25美分的个数：");
   a=scan.nextInt();

   System.out.println("请您输入10美分的个数: ");
   b=scan.nextInt();

   System.out.println("请您输入5美分的个数： ");
   c=scan.nextInt();

   System.out.println("请您输入1美分的个数： ");
   d=scan.nextInt();
  
    e=(25*a+10*b+5*c+d)/A;
    f=(25*a+10*b+5*c+d)%A;
   
    System.out.println("您的存钱罐里硬币的总面值为： "+e+"美元"+f+"美分。");
   
    }
}
