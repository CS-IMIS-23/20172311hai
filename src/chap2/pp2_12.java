//*****************************************************************************************************************
// Calculate.java                       作者：赵晓海
//
//用户在该程序中输入一个整数作为正方形边长，就会输出该正方形的周长和面积。
//
//***************************************************************************************************************

  import java.util.Scanner;
 
  class pp2_12
  {
  
  public static void main(String [] args)
  { 
  
  int a,b,c;//  a是输入的整数，b为正方形周长，c为正方形面积。

  Scanner scan=new Scanner(System.in);

  System.out.println("请您输入一个正整数作为在正方形边长： ");
   a=scan.nextInt();

   b=4*a;
   c=a*a;
  
   System.out.println("该正方形的周长为： "+b+"\n该正方形的面积为： "+c);
   
   }

}
