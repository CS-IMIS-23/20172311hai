//********************************************************************
//Time Computation           作者：赵晓海
//
//用于计算 时间=路程/速度
//********************************************************************

  import java.util.Scanner;

  public class pp2_7
  {
  public static void main(String [] args)
  {
   int length,speed;
   double time;
   Scanner scan=new Scanner(System.in);

   System.out.println("我们将计算您旅行所需的时间，请您输入旅行距离的整型值： ");
   length=scan.nextInt();

   System.out.println("请您输入您旅行速度的整型值： ");
   speed=scan.nextInt();

   time =(double)length/speed;

   System.out.println("您的旅行时间为： "+time);

    }
}

