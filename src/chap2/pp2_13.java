//*****************************************************************
//  pp2_13.java               作者：赵晓海
//
//  输出一个分数的小数形式。
//*****************************************************************

   import java.util.Scanner;
   
   public class pp2_13
   {
   public static void main(String [] args)
   {
   int a,b;// a代表分子，b代表分母
   
   double c;  //c=a/b
   
   Scanner scan=new Scanner(System.in);
   
   System.out.println("你输入的分数将被转换为小数形式，请您输入分子部分： ");
   a=scan.nextInt();
   
   System.out.println("请您输入分母部分： ");
   b=scan.nextInt();

   c=(double)a/b;
   
   System.out.println("a/b= "+c);
  
   }
}



























