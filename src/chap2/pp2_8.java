//*******************************************************
// Time Transform.java        作者：赵晓海
//
//  将时间转换为以秒表示。
//*******************************************************

  import java.util.Scanner;

  public class pp2_8
  {
   public static void main(String [] args)
  {
   int a,b,c,d;
   final int A=3600,B=60;
  
   Scanner scan=new Scanner(System.in);
   
   System.out.println("你输入的时间将被转换为以秒为单位，请您先输入小时数： ");
   a=scan.nextInt();            //a代表小时数

   System.out.println("请您继续输入分钟数： ");
   b=scan.nextInt();            //b代表分钟数
   
   System.out.println("请您继续输入秒数： ");
   c=scan.nextInt();           //c代表秒数
   
   d=3600*a+60*b+c;

    System.out.println(a+"小时"+b+"分钟"+c+"秒= "+d+"秒");

    }
}
