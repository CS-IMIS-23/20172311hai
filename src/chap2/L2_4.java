//*****************************************************************************************
// Roses.java      Author:zhaoxiaohai

//Demonstrate the use of escape sequences.
//*****************************************************************************************

 class L2_4
{
   //-----------------------------------------------------------------------------------
   //Prints a poem (of sorts) on multiple lines.
   //---------------------------------------------------------------------------------
   public static void main (String [] args)
   {
    System.out.println("Roses are red,\n\tViolets are blue,\n"+
            "Suger is sweet,\n\tBut I have \"commitment issures\",\n\t" +
            "So I'd rather be friends\n\tAt this point in our " + 
      "relationship.");
   }
}
