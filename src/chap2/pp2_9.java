//****************************************************************
//Time Transform.java                作者：赵晓海
//  
// 将以秒为单位的时间转换为 小时 分钟 秒的形式。
//****************************************************************


  import java.util.Scanner;

  public class pp2_9
  {
  public static void main(String [] args)
  {
   int a,b,c,d,e;
   final int A=3600,B=60;

   Scanner scan=new Scanner(System.in);
   
   System.out.println("您输入的秒数将被转换为\"小时+分钟+秒\"的形式，请您输入秒数： ");
   a=scan.nextInt();//a代表输入的秒数
  
   b=a/A;//b代表小时数
   c=a%A;//c代表第一次余下的秒数
   d=c/B;//d代表分钟数
   e=c%B;//e代表第二次余下的秒数
   
   System.out.println(a+"秒= "+b+"小时"+d+"分钟"+e+"秒");

   }
}
